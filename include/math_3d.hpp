/**
 * @file math_3d.hpp
 *
 */
#pragma once
#include <glm/ext.hpp>
#include <iostream>
#include <vector>
#include <map>
#include <stack>
#include <glm/glm.hpp>

using glm::vec2;
using glm::vec3;
using glm::vec4;
using glm::ivec2;
using glm::ivec3;
using glm::ivec4;
using glm::mat2;
using glm::mat4;
using glm::mat3;
using glm::dot;
using glm::cross;
using glm::translate;
using glm::rotate;
using glm::scale;
using glm::ortho;
using glm::perspective;
using glm::degrees;
using glm::radians;

using std::vector;
using std::endl;
using std::cout;
using std::string;
using std::numeric_limits;
using std::stringstream;
using std::pair;
using std::make_pair;
using std::map;
using std::min;
using std::max;
using std::stack;

/*!
* @brief @public Find a vector that is orthogonal to v.
*/
vec3 findOrthogonal(vec3 v);
/*!
* @brief @public Given the vector, v, generate two other orthogonal vectors thus creating a coordinate frame.
*/
mat3 formOrthogonalBasis(vec3 const &v);
/*!
* @brief @public Normalize all vectors in the matrix, m.
*/
mat3 orthonormalizeOrientation(mat3 m);
/*!
* @brief @public Return a skew-symmetric (cross product matrix) for the vector, v.
*/
mat3 skewSymmetric(vec3 const &v);
/*!
* @brief @public Get the 6 vertices that define the corners of the AABB defined by the min and max points.
*/
vector<vec3> getAABBVertices(const vec3 &_min, const vec3 &_max);
/*!
* @brief @public Clamp the value between the min and max values.
*/
float clamp(float value, float _min, float _max);
/*!
* @brief @public Multiply a matrix with a vec3.
*/
vec3 matrixMultiply(const mat4 &m, const vec3 &v);
/*!
* @brief @public Return a vector of floats representing the min and max values (minX, minY, minZ, maxX, maxY, maxZ) of the input vertices.
*/
vector<float> getAABB(const vector<vec3> &vertices);
/*!
* @brief @public AABB collision detection.
*/
bool AABBOverlap(const vec3 &_min1, const vec3 &_max1, const vec3 &_min2, const vec3 &_max2);
/*!
* @brief @public Point and line collision, @todo what does this do?
*/
float pointLineIntersection(const vec3 &p3, const vec3 &p1, const vec3 &p2);
/*!
* @brief @public Distance from a point to a triangle.
*/
float distPointToTriangle(const vec3 &point, const vec3 &v1, const vec3 &v2, const vec3 &v3);
/*!
* @brief @public Collision detection between two line segments, returns t1,t2, where the closest points on each line are p1 + t1 * p2, p3 + t2 * p4  
*/
vec2 lineSegmentIntersection(const vec3 &p1, const vec3 &p2, const vec3 &p3, const vec3 &p4);
/*!
* @brief @public Collision detection between two line segments, returns t1,t2, where the closest points on each line are p1 + t1 * p2, p3 + t2 * p4  
*/
vec2 closestPointsOnLineSegments(const vec3 &p1, const vec3 &p2, const vec3 &p3, const vec3 &p4);
/*!
* @brief @public Distance between a point and a plane.
*/
float pointPlaneDistance(const vec3 &point, const vec3 &planePosition, const vec3 &planeNormal);
/*!
* @brief @public Closest point on a line segment to a point.
*/
vec3 pointLineSegmentIntersection(vec3 p, vec3 l1, vec3 l2);
/*!
* @brief @public Closest point on a quad to a point.
*/
vec3 pointQuadIntersection(vec3 pt, vec3 t1, vec3 t2, vec3 t3, vec3 t4);
/*!
* @brief @public Closest point on a triangle to a point.
*/
vec3 pointTriangleIntersection(vec3 pt, vec3 t1, vec3 t2, vec3 t3, unsigned int &type);
/*!
* @brief @public Sutherland-Hodgeman clipping algorithm.
*/
bool clipPolygonToPlane(vector<vec3> &polygon, const vec3 &plane_pos, const vec3 &plane_normal, vector<vector<int> > &contactIDs, unsigned int planeIndex);
/*!
* @brief @public Minkowski difference to early out some checks in SAT.
*/
bool buildMinkowskiFace(const vec3 &a, const vec3 &b, const vec3 &b_x_a, const vec3 &c, const vec3 &d, const vec3 &d_x_c);
/*!
* @brief @public Return closest point on a circle to a point, p.
*/
vec3 closestPointOnCircle(vec3 p, vec3 plane_position, vec3 plane_normal, float r);
/*!
* @brief@public  Sphere plane collision detection.
*/
float spherePlaneCollision(vec3 sphereCentre, vec3 planeNormal, vec3 pointOnPlane);
/*!
* @brief @public Ray triangle collision detection.
*/
bool rayTriangleCollision(vec3 planeIntersection, vec3 planeNormal, vec3 v1, vec3 v2, vec3 v3);
/*!
* @brief @public closest point on line segment, A, B, to point.
*/
vec3 closestPointOnLineSegment(vec3 A, vec3 B, vec3 point);
/*!
* @brief @public Returns closest point on OBB, from point.
*/
vec3 closestPointOnOBB(vec3 point, float box_dX2, float box_dY2, float box_dZ2, vec3 box_position, mat3 box_orientation);
/*!
* @brief @public Parallel Axis Theorem (Nyguen-Steiner)
*/
mat3 parallelAxis(vec3 r);
/*!
* @brief @public Get position from a model matrix.
*/
vec3 getPos(mat4 m);