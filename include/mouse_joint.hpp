/**
 * @file mouse_joint.hpp
 *
 */
#pragma once

#include "body.hpp"

/**
 * @brief Mouse Joint, specifying an anchor in world-space and one on the body, will constrain the body at those points.
 * @todo change CollisionBody to RigidBody
 * @todo improve the prototype
 */
class MouseJoint  {
public:

    vec3 bias = vec3(0,0,0);
    mat3 effective_mass = mat3(0.0f);
    vec3 P = vec3(0,0,0);
    
    CollisionBody *body_a;
    CollisionBody *body_b;

    vec3 anchor_a;
    vec3 anchor_b;

    vec3 anchor_a_ws;
    vec3 anchor_b_ws;
    vec3 prev_anchor_b = vec3(0,0,0);

    unsigned int body_a_index;
    unsigned int body_b_index;

    bool unset = true;

    MouseJoint(unsigned int indexA, vec3 anchorA, vec3 anchorB);

    int setAnchor(vec3 anchor);
    int setBodyAnchor(vec3 localPoint);

    int applyImpulse();

    int preStep(float dt);

    static vector<MouseJoint*> allJoints;
};