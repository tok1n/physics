/**
 * @file body.hpp
 *
 */
#pragma once
#include <stack>
#include <algorithm>

#define GLM_ENABLE_EXPERIMENTAL
#include "glm/glm.hpp"
#include "glm/gtx/string_cast.hpp"
#include "glm/gtx/norm.hpp"
class CollisionBody;
#include "math_3d.hpp"
#include "collision.hpp"

const float pi = 3.141592654f;

/*!
 * @brief Face structure for organising CollisionBody collision data.
 */
struct Face {
    vector<vec3> verts_ws = {};
    vector<uint> verts_indices = {};
    vec3 position_ws;
    vec3 normal_ws;
    int id = -1;
    /*!
    * @brief Face constructor, takes a vector of vertex indexes into the CollisionBody.
    */
    Face(const vector<uint> &verts_indices);
    /*!
    * @brief Set Face vertex data.
    */
    int setData(const vector<vec3> &verts);
    /*!
    * @brief Face copy constructor.
    */
    Face(const Face &f);
    ~Face();

};

/*!
 * @brief Edge structure for organising CollisionBody collision data.
 */
struct Edge {
    vec3 v1_ws;
    vec3 v2_ws;

    int v1_index;
    int v2_index;

    vector<uint> faces_indices = {};
    vector<Face> faces = {};
    int id = -1;
    /*!
    * @brief Face constructor, takes a vector of vertex indexes into the CollisionBody.
    */
    Edge(const vector<uint> &verts_indices, const vector<uint> &faces_indices);
    ~Edge();
    /*!
    * @brief Edge constructor, takes a vector of vertices and the associated (2) Face's. @todo make these pointers.
    */
    int setData(const vector<vec3> &verts, const vector<Face> &faces);
};

enum BodyType { NONE=-1, TRIANGLE=0, BOX=1, SPHERE=2, CYLINDER=3, COMPOSITE=4};

/*!
 * @brief CollisionBody structure for organising data to model physics simulations of convex hull collisions.
 */
class CollisionBody {
public:
    /*!
    * @brief Default constructor, CollisionBody needs to be init{Type} with one of the inititalization functions.
    */
    CollisionBody();
    CollisionBody& operator=(const CollisionBody &other);
    CollisionBody(const CollisionBody &other);
    ~CollisionBody();

    /*!
    * @brief Getter for model matrix, only the integrator can update the model matrix.
    */
    mat4 getModelMatrix();
    /*!
    * @brief Updates the collision data for the current frame.
    */
    int update();
    /*!
    * @brief Calculates world space vertex data, for collision detection.
    */
    int calculateVertices();
    /*!
    * @brief Get index in CollisionBody.allBodies for the ID.
    */
    static int getIndex(int ID);

public:
    static vector<CollisionBody*> allBodies;
    static int Counter;
    float dX2 = 0.5f;
    float dY2 = 0.5f;
    float dZ2 = 0.5f;

    float bounding_radius;

    BodyType type = BodyType::NONE;
    int id = -1;
    vector<int> triIDs = {};
    vector<int> objectIDs = {};

    bool colliding;

    float coefficient_of_restitution;
    float mass;
    float one_over_mass;
    float density = 1.0f;
    float friction;
    vec3 cm_position;
    mat3 orientation;
    vec3 cm_velocity;
    vec3 angular_momentum;
    vec3 cm_force;
    vec3 torque;
    mat3 inverse_world_inertia_tensor;
    mat3 inverse_body_inertia_tensor;
    vec3 angular_velocity;

    vector<vec3> a_bounding_vertices = { vec3(0,0,0),vec3(0,0,0),vec3(0,0,0),vec3(0,0,0),
                                         vec3(0,0,0),vec3(0,0,0),vec3(0,0,0),vec3(0,0,0) };

    vector<vec3> a_body_bounding_vertices = {};
    vector<Face> faces = {};
    vector<Edge> edges = {};

    mat4 offset;

    void *contactFeature = NULL;
    int compositeBodyIndex = -1;

    bool sleeping = false;

    int group_id = -1;

    bool is_static = false;
};

/*!
 * @brief Initializer function for a box CollisionBody; w x h x b
 */
int initBox(CollisionBody *body, float w, float h, float b);
/*!
 * @brief Initializer function for a sphere CollisionBody.
 */
int initSphere(CollisionBody *body, float r);
/*!
 * @brief Initializer function for a cylinder CollisionBody.
 */
int initCylinder(CollisionBody *body, float r1, float r2, float h);
/*!
 * @brief Initializer function for a triangle CollisionBody.
 */
int initTriangle(CollisionBody *body, vector<vec3> verts);

/*!
 * @brief RigidBody structure for organising data to model physics simulations of convex hull collisions.
 */
class RigidBody {
public:
    /*!
    * @brief Default constructor, RigidBody needs to be init{Type} with one of the inititalization functions.
    */
    RigidBody();
    /*!
    * @brief Destructor.
    */    
    ~RigidBody();
    /*!
    * @brief Method for integrating all the forces for a timestep.
    */
    int integrateForces(vec3 gravity, float deltaTime);
    /*!
    * @brief Method for integrating the velocities for a timestep.
    */
    int integrateVelocities(float deltaTime);
    /*!
    * @brief Add a CollisionBody to RigidBody.
    */
    void add(int bodyID);
    /*!
    * @brief Getter for model matrix, only the integrator can update the model matrix.
    */
    mat4 getModelMatrix();
    /*!
    * @brief Getter for whether RigidBody is colliding or not.
    */
    bool getColliding();

public:

    static int Counter;

    int id = -1;

    float coefficient_of_restitution;
    float mass;
    float one_over_mass;
    float density = 1.0f;
    vec3 cm_position;
    mat3 orientation;
    vec3 cm_velocity;
    vec3 angular_momentum;
    vec3 cm_force;
    vec3 torque;
    mat3 inverse_world_inertia_tensor;
    mat3 inverse_body_inertia_tensor;
    vec3 angular_velocity;

    vector<vec3> a_bounding_vertices = { vec3(0,0,0),vec3(0,0,0),vec3(0,0,0),vec3(0,0,0),
                                         vec3(0,0,0),vec3(0,0,0),vec3(0,0,0),vec3(0,0,0) };

    vector<vec3> a_body_bounding_vertices = {};

    vector<unsigned int> bodyIndexes = {};
    static vector<RigidBody*> allBodies;
    int compositeBodyIndex = -1;
    BodyType type;

    // default false for stability
    bool apply_gyroscopic_term = false;

    bool sleeping = false;
    unsigned int colliding = 0;
};