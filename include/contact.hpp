/**
 * @file contact.hpp
 */
#pragma once

#include "body.hpp"
#include "spherical_joint.hpp"
#include "hinge_joint.hpp"
#include "mouse_joint.hpp"
#include "collision.hpp"

enum ContactType { FACE=0, EDGE=1};

/*!
* @brief A data structure to pass data received from a collision to the solver.
*/
class Contact {
public:
    vec3 normal = vec3(0,0,0);
    vec3 tangent = vec3(0,0,0);
    vec3 bitangent = vec3(0,0,0);
    vec3 collision_point;
    float penetration = 0;
    float bias = 0;
    float effective_mass_bitangent = 0.0f;
    float effective_mass_tangent = 0.0f;
    float effective_mass_normal = 0.0f;
    float Pn = 0;
    float Pt = 0;
    float Pbt = 0;
    RigidBody *body_a;
    RigidBody *body_b;

    CollisionBody *col_body_a;
    CollisionBody *col_body_b;

    ContactType type;
    vector<int> id;
    int warmStartNo = 0;
    /*!
    * @brief Constructor, instantiated with the collision query between two CollisionBody objects.
    */
    Contact(CollisionBody &hullA, CollisionBody &hullB);
    /*!
    * @brief Impulse-solver function, applies both normal impulse and friction impulses.
    */
    int applyImpulse();
    /*!
    * @brief Apply impulse from previous frame for better coherence.
    */
    int preStep(float dt);

    static vector<Contact> allContacts;
    static unsigned int iterations;

};

/*!
* @brief @public Get contacts for a CollisionBody.
*/
vector<Contact> getContacts(CollisionBody &body);
/*!
* @brief @public Find the Face on the CollisionBody that is most parallel to the vector, normal.
*/
vector<Face> getMostParallelFace(const CollisionBody &hull, vec3 normal);
/*!
* @brief @public Find the Face on the CollisionBody that is most anti-parallel to the vector, normal.
*/
vector<Face> getMostAntiparallelFace(const CollisionBody &hull, vec3 normal);
/*!
* @brief @public Find the Face's that are orthogonal to the vector, normal, used for clipping to generate contact points.
*/
vector<Face> getOrthogonalFaces(const CollisionBody &hull, vec3 normal);
/*!
* @brief @public A data structure to contain the data received from a collision between a sphere and a sphere.
*/
vector<Contact> sphereSphereContacts(CollisionBody &body_a,  CollisionBody &body_b);
/*!
* @brief @public A data structure to contain the data received from a collision between a sphere and a tri.
*/
vector<Contact> sphereTriContacts(CollisionBody &body_a,  CollisionBody &body_b);
/*!
* @brief @public A data structure to contain the data received from a collision between a sphere and a box.
*/
vector<Contact> sphereBoxContacts(CollisionBody &body_a,  CollisionBody &body_b);
/*!
* @brief @public A data structure to contain the data received from a collision between a box and a box.
*/
vector<Contact> hullHullContacts(CollisionBody &body_a,  CollisionBody &body_b);
/*!
* @brief @public A data structure to contain the data received from a collision between a box and a box.
*/
vector<Contact> boxCylinderContacts(CollisionBody &body_a,  CollisionBody &body_b);
/*!
* @brief @public A data structure to contain the data received from a collision between a Sphere and a Cylinder.
*/
vector<Contact> sphereCylinderContacts(CollisionBody &body_a,  CollisionBody &body_b);
/*!
* @brief @public A data structure to contain the data received from a collision between a Cylinder and a Cylinder.
*/
vector<Contact> cylinderCylinderContacts(CollisionBody &body_a,  CollisionBody &body_b);
/*!
* @brief @public Generate contacts between two CollisionBody's.
*/
vector<Contact> createContacts(CollisionBody &body_a,  CollisionBody &body_b);
/*!
* @brief @public Reduce a vector of Contact's to 4 that are the most stable.
*/
vector<Contact> createMinimalContacts(vector<Contact> &contacts);
/*!
* @brief @public Return the point on a tri that is closest to a point.
*/
vec3 closestPointOnTri(vec3 sphereCentre, vec3 p0, vec3 p1, vec3 p2);
/*!
* @brief @public Return collision points on each CollisionBody.
*/
vector<vec3> collisionPoints(CollisionBody &body_a, CollisionBody &body_b);
/*!
* @brief @public Access function for the physics time step function, called on the client-side manually in the update function.
*/
int simulate(float deltaTime);

vector<Contact> GJKEPAContacts(CollisionBody &body_a, CollisionBody &body_b);


vector<Contact> sphereTriContacts(CollisionBody &body_a, CollisionBody &body_b);