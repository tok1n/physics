/**
 * @file hinge_joint.hpp
 *
 */
#pragma once

#include "body.hpp"

/*!
 * @brief Hinge Joint, a position constraint, and an orthogonality constraint.
 *
 */
class HingeJoint {
public:
    // position constraint state
    vec3 bias = vec3(0,0,0);
    mat3 effective_mass = mat3(0.0f);
    vec3 P = vec3(0,0,0);

    // angle constraint state
    vec2 bias2 = vec3(0,0,0);
    mat2 effective_mass2 = mat2(0.0f);
    vec2 P2 = vec2(0,0);

    RigidBody *body_a;
    RigidBody *body_b;

    vec3 anchor_a;
    vec3 anchor_b;

    vec3 anchor_a_ws;
    vec3 anchor_b_ws;

    float dt = 0.02f;

    unsigned int body_a_index;
    unsigned int body_b_index;

    unsigned int axisA;
    unsigned int axisB;
    unsigned int axisC;

    bool unset = true;
    /*!
     * @brief Hinge Joint, a position constraint, and an orthogonality constraint.
     * \param[in]      indexA           ID of the first RigidBody.
     * \param[in]      anchorA          Local position on the first RigidBody.
     * \param[in]      indexB          ID of the second RigidBody.
     * \param[in]      anchorB           Local position on the second RigidBody.
     * \param[in]      axisA            Vector on the first RigidBody. (0, 1 or 2, for x,y,z respectively).
     * \param[in]      axisB            First Orthogonal vector on the second RigidBody. (0, 1 or 2, for x,y,z respectively).
     * \param[in]      axisC            Second Orthgonal vector on the second RigidBody. (0, 1 or 2, for x,y,z respectively).
     */
    HingeJoint(unsigned int indexA, vec3 anchorA,  unsigned int indexB,  vec3 anchorB, unsigned int axisA, unsigned int axisB, unsigned int axisC);

    int applyImpulse();

    int preStep(float dt);

    static vector<HingeJoint*> allJoints;
};