/**
 * @file collision.hpp
 *
 */
#pragma once
#include "body.hpp"

extern float depth_epsilon;   

/*!
* @brief Find furthest vertex along normal, n, and return its index into hull.a_bounding_vertices.
*/
unsigned int getSupport(CollisionBody &hull, vec3 n);


enum QueryType { EDGE_AB=0, FACE_AB=1, FACE_BA=2 };
/*!
* @brief A data structure to get data received from a collision.
*/
struct Query {
    float best_distance = -FLT_MAX;
    glm::ivec2 collision_pair_index = glm::ivec2(-1,-1);
    glm::vec3 normal;
    int id = -1;
   /*!
    * @brief Default Constructor
    */  
    Query(){};
    /*!
    * @brief Given point p, return point q on (or in) OBB b, closest to p
    */
    Query(float _best_distance, glm::ivec2 _collision_pair_index, glm::vec3 _normal) {
        best_distance = _best_distance;
        collision_pair_index = _collision_pair_index;
        normal = _normal;
        id = -1;
    }
    Query(const Query &q) {
        this->best_distance = q.best_distance;
        this->collision_pair_index = q.collision_pair_index;
        this->normal = q.normal;
        this->id = q.id;
    }
    Query& operator=(const Query &q) {
        this->best_distance = q.best_distance;
        this->collision_pair_index = q.collision_pair_index;
        this->normal = q.normal;
        this->id = q.id;
        return *this;
    }
};
/*!
* @brief Check collision between a BodyType::CYLINDER CollisionBody and a BodyType::BOX CollisionBody.
*/
bool cylinderBoxCollision(const CollisionBody &cyl, const CollisionBody &obb);
/*!
* @brief Check collision between a CollisionBody and a sphere.
*/
bool sphereBoxCollision(const CollisionBody &hullA, vec3 closest_pt);
/*!
* @brief Check collision between a sphere and a sphere.
*/
bool sphereSphereCollision(const CollisionBody &hullA, const CollisionBody &hullB);
/*!
* @brief Check collision between a sphere and a cylinder.
*/
bool sphereCylinderCollision(const CollisionBody &body_a, vec3 closest_pt);
/*!
* @brief Check collision between a CollisionBody and a CollisionBody.
*/
bool SAT(CollisionBody &hullA, CollisionBody &hullB, Query &query);
/*!
* @brief Return the closest point on a cylinder, to another point, p.
*/
vec3 closestPointOnCylinder(vec3 p, const CollisionBody &cylinder);
/*!
* @brief @todo Query for Face collisions in SAT. 
*/
Query queryFaceDirections(CollisionBody &hullA, CollisionBody &hullB);
/*!
* @brief Query for Edge collisions in SAT.
*/
Query queryEdgeDirections(CollisionBody &hullA, CollisionBody &hullB);
/*!
* @brief Ray OBB intersection function, returns a float along the vector origin + t * direction = contact_point.
*/
float rayOBBIntersect(vec3 origin, vec3 direction, mat3 obb_orientation);
