/**
 * @file spherical_joint.hpp
 *
 */
#pragma once

#include "body.hpp"

/**
 * @brief Ball-in-socket Joint, specifying an anchor on each body will constrain the two bodies at those points.
 *
 */
class SphericalJoint  {
public:

    vec3 bias = vec3(0,0,0);
    mat3 effective_mass = mat3(0.0f);
    vec3 P = vec3(0,0,0);
    
    RigidBody *body_a;
    RigidBody *body_b;

    vec3 anchor_a;
    vec3 anchor_b;

    vec3 anchor_a_ws;
    vec3 anchor_b_ws;

    float dt = 0.02f;

    unsigned int body_a_index;
    unsigned int body_b_index;

    bool unset = true;

    SphericalJoint(unsigned int indexA, vec3 anchorA,  unsigned int indexB,  vec3 anchorB);

    int applyImpulse();

    int preStep(float dt);

    static vector<SphericalJoint*> allJoints;
};