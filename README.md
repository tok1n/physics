# Physics

![example](example.gif "example")

A 3D physics engine

* Uses iterative sequential impulse method, with accumulated impulses and warm starting, popularized by Catto in Box2D
* Spheres, boxes, cylinders, capsules*, triangle meshes, joints.

## Build

clone with:
```
git clone https://codeberg.org/tok1n/physics.git
```

Run

```
make
```
from the root directory to build the library.

## Basics

- Include [physics.hpp](https://tok1n.codeberg.page/physics/docs/html/physics_8hpp_source.html) and link to libphysics.so
- Create a CollisionBody, and initialize with something like initBox()
- Add one or more CollisionBody's to a RigidBody to act as one body
- Call the simulate() function to run the simulation
- Loop through the static CollisionBody::allBodies vector to retrieve the updated orientation and position with CollisionBody::getModelMatrix() and set it to your own supplied renderer's (or you could use [engine](https://codeberg.org/tok1n/engine)!) mesh model matrix and it will draw the correctly simulating bodies

## Docs

[Go to documentation](https://tok1n.codeberg.page/physics/docs/html/md__r_e_a_d_m_e.html)
