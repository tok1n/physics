#include "hinge_joint.hpp"

#define GLM_ENABLE_EXPERIMENTAL
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/string_cast.hpp>

vector<HingeJoint*> HingeJoint::allJoints = {};
HingeJoint::HingeJoint(unsigned int indexA, vec3 anchorA,  unsigned int indexB,  vec3 anchorB, unsigned int _axisA, unsigned int _axisB, unsigned int _axisC) {
    body_a_index = indexA;
    body_b_index = indexB;
    anchor_a = anchorA;
    anchor_b = anchorB;
    anchor_a_ws = anchorA;
    anchor_b_ws = anchorB;
    unset = true;
    // currently only allow orthgonal local space hinges.
    assert(_axisA >= 0 && _axisA <= 2);
    assert(_axisB >= 0 && _axisB <= 2);
    assert(_axisC >= 0 && _axisC <= 2);
    axisA = _axisA;
    axisB = _axisB;
    axisC = _axisC;
    HingeJoint::allJoints.push_back(this);
}

int HingeJoint::preStep(float dt) {

    body_a = RigidBody::allBodies.at(body_a_index);
    body_b = RigidBody::allBodies.at(body_b_index);

    vec3 pa = body_a->cm_position + body_a->orientation * this->anchor_a;
    vec3 ra = pa - body_a->cm_position;
    vec3 pb = body_b->cm_position + body_b->orientation * this->anchor_b;
    vec3 rb = pb - body_b->cm_position;

    float k_biasFactor = 0.1f;
    this->bias = -k_biasFactor * 1.0f / dt * (pb - pa);

    effective_mass = (body_a->one_over_mass + body_b->one_over_mass) * mat3(1.0f)
                       - skewSymmetric(ra) * body_a->inverse_world_inertia_tensor * skewSymmetric(ra)
                       - skewSymmetric(rb) * body_b->inverse_world_inertia_tensor * skewSymmetric(rb);

    effective_mass = glm::inverse(effective_mass);

    // rotation constraint
    vec3 a1 = normalize(body_a->orientation[axisA]);
    vec3 b2 = normalize(body_b->orientation[axisB]);
    vec3 c2 = normalize(body_b->orientation[axisC]);
    
    vec3 impulse2 = cross(b2, a1) * P2.x + cross(c2, a1) * P2.y;

    this->bias2 =  -k_biasFactor * 1.0f / dt * vec2(dot(a1,b2), dot(a1,c2));
    // K matrix
    vec3 Ia_b2_x_a1 = cross(b2, a1) * body_a->inverse_world_inertia_tensor;
    vec3 Ia_c2_x_a1 = cross(c2, a1) * body_a->inverse_world_inertia_tensor;
    vec3 Ib_b2_x_a1 = cross(b2, a1) * body_b->inverse_world_inertia_tensor;
    vec3 Ib_c2_x_a1 = cross(c2, a1) * body_b->inverse_world_inertia_tensor;

    float a = dot(Ia_b2_x_a1, cross(b2, a1)) + dot(Ib_b2_x_a1, cross(b2, a1));
    float b = dot(Ia_c2_x_a1, cross(b2, a1)) + dot(Ib_c2_x_a1, cross(b2, a1));
    float c = dot(Ia_b2_x_a1, cross(c2, a1)) + dot(Ib_b2_x_a1, cross(c2, a1));
    float d = dot(Ia_c2_x_a1, cross(c2, a1)) + dot(Ib_c2_x_a1, cross(c2, a1));

    effective_mass2 = glm::inverse(mat2(a,b,c,d));

    // apply impulse to primary quantities
    body_a->cm_velocity -= P * body_a->one_over_mass;
    body_a->angular_momentum -= cross(ra, P) + impulse2;
    // compute affected auxillary quantities
    body_a->angular_velocity = body_a->inverse_world_inertia_tensor * body_a->angular_momentum;

    // apply impulse to primary quantities
    body_b->cm_velocity += P * body_b->one_over_mass;
    body_b->angular_momentum += cross(rb, P) + impulse2;
    // compute affected auxillary quantities
    body_b->angular_velocity = body_b->inverse_world_inertia_tensor * body_b->angular_momentum;


    return 0;
}

int HingeJoint::applyImpulse() {

    vec3 pa = body_a->cm_position + body_a->orientation * this->anchor_a;
    vec3 ra = pa - body_a->cm_position;
    vec3 va = body_a->cm_velocity + cross(body_a->angular_velocity, ra);
    vec3 pb = body_b->cm_position + body_b->orientation * this->anchor_b;
    vec3 rb = pb - body_b->cm_position;
    vec3 vb = body_b->cm_velocity + cross(body_b->angular_velocity, rb);

    anchor_a_ws = pa;
    anchor_b_ws = pb;

    vec3 rel_vel = vb - va;
    vec3 impulse = effective_mass * (-rel_vel + this->bias);

    vec3 a1 = normalize(body_a->orientation[axisA]);
    vec3 b2 = normalize(body_b->orientation[axisB]);
    vec3 c2 = normalize(body_b->orientation[axisC]);

    float x2;
    float y2;
    float x1;
    float y1;

    x2 = dot(cross(b2, a1), body_a->angular_velocity);
    y2 = dot(cross(c2, a1), body_a->angular_velocity);

    x1 = dot(cross(b2, a1), body_b->angular_velocity);
    y1 = dot(cross(c2, a1), body_b->angular_velocity);

    vec2 rel_vel2 = vec2(x1 - x2, y1 - y2);

    vec2 dv = effective_mass2 * (-rel_vel2 + this->bias2 );

    vec3 impulse2 = cross(b2, a1) * dv.x + cross(c2, a1) * dv.y;

    // apply impulse to primary quantities
    body_a->cm_velocity -= impulse * body_a->one_over_mass;
    body_a->angular_momentum -= cross(ra, impulse) + impulse2;
    // compute affected auxillary quantities
    body_a->angular_velocity = body_a->inverse_world_inertia_tensor * body_a->angular_momentum;

    // apply impulse to primary quantities
    body_b->cm_velocity += impulse * body_b->one_over_mass;
    body_b->angular_momentum += cross(rb, impulse) + impulse2;
    // compute affected auxillary quantities
    body_b->angular_velocity = body_b->inverse_world_inertia_tensor * body_b->angular_momentum;

    // accumulate impulses
    P += impulse;
    P2 += dv;


    return 0;
}