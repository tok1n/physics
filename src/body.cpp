#include "body.hpp"

int CollisionBody::Counter = -1;
vector<CollisionBody*> CollisionBody::allBodies = {};

CollisionBody::CollisionBody() {
    offset = mat4(1.0f);
    coefficient_of_restitution = 0.7f;
    density = 0.4f;
    friction = 1.0f;
    cm_position = vec3(0,0,0);
    orientation = mat3(vec3(1,0,0), vec3(0,1,0), vec3(0,0,1));
    cm_velocity = vec3(0,0,0);
    angular_momentum = vec3(0,0,0);
    cm_force = vec3(0,0,0);
    torque = vec3(0,0,0);
    inverse_world_inertia_tensor = mat3(vec3(1,0,0), vec3(0,1,0), vec3(0,0,1));
    inverse_body_inertia_tensor = mat3(vec3(1,0,0), vec3(0,1,0), vec3(0,0,1));
    angular_velocity = vec3(0,0,0);
    compositeBodyIndex = -1;
    mass = 1.0f;
    one_over_mass = 1.0f / mass;   
    id = -1;
};

CollisionBody& CollisionBody::operator=(const CollisionBody &other) {
    //if (other.type == BodyType::TRIANGLE) {
        cout << "assignment operator triangle!" << endl;
    //}
    return *this;
}

CollisionBody::CollisionBody(const CollisionBody &other) {
    //if (other.type == BodyType::TRIANGLE) {
        cout << "copying triangle!" << endl;
   //}
    id = other.id;
    offset = other.offset;
    coefficient_of_restitution = other.coefficient_of_restitution;
    density = other.density;
    friction = other.friction;
    cm_position = other.cm_position;
    orientation = other.orientation;
    cm_velocity = other.cm_velocity;
    angular_momentum = other.angular_momentum;
    cm_force = other.cm_force;
    torque = other.torque;
    inverse_world_inertia_tensor = other.inverse_world_inertia_tensor;
    inverse_body_inertia_tensor = other.inverse_body_inertia_tensor;
    angular_velocity = other.angular_velocity;
    compositeBodyIndex = other.compositeBodyIndex;
    mass = other.mass;
    one_over_mass = other.one_over_mass;
    faces = other.faces;
    edges = other.edges;
    type = other.type;
    inverse_body_inertia_tensor = other.inverse_body_inertia_tensor;
    inverse_world_inertia_tensor = other.inverse_world_inertia_tensor;
    dX2 = other.dX2;
    dY2 = other.dY2;
    dZ2 = other.dZ2;
    a_bounding_vertices = other.a_bounding_vertices;
    a_body_bounding_vertices = other.a_body_bounding_vertices;
    if (other.type == BodyType::TRIANGLE) {
        cout << a_bounding_vertices.size() << endl;
    }
}

int CollisionBody::getIndex(int ID) {
    // @todo  make more efficient
    for (unsigned int i = 0; i < CollisionBody::allBodies.size(); i++) {
        if (CollisionBody::allBodies.at(i)->id == ID) {
            return i;
        }
    }
    return -1;
}

CollisionBody::~CollisionBody() {

}

mat4 CollisionBody::getModelMatrix() {
    mat4 m = translate(mat4(1.0f), cm_position) * mat4(orientation);
    m = scale(m, vec3(dX2, dY2, dZ2));
    m = m;
    return m;
}

int CollisionBody::update() {
    calculateVertices();
    for (unsigned int i = 0; i < faces.size(); i++) {

        Face *f = &faces.at(i);
        f->setData(a_bounding_vertices);
        // if TRIANGLE, recalculate edge face normals
/*        if (type == BodyType::TRIANGLE && i > 1) {
            f->normal_ws = normalize(cross(faces.at(0).normal_ws, f->verts_ws.at(1) - f->verts_ws.at(0)));
            if (dot(f->verts_ws.at(0) - faces.at(0).position_ws, f->normal_ws) < 0.0f) {
                f->normal_ws = f->normal_ws * -1.0f;
            }
        }*/
    }

    for (unsigned int i = 0; i < edges.size(); i++) {
        Edge *e = &edges.at(i);
        e->setData(a_bounding_vertices, faces);
    }

    return 0;
}

int CollisionBody::calculateVertices() {
    a_bounding_vertices = {};
    for (unsigned int i = 0; i < a_body_bounding_vertices.size(); i++) {
        a_bounding_vertices.push_back(cm_position + orientation * a_body_bounding_vertices.at(i));
    }
    return 0;
}

Face::Face(const vector<uint> &_verts_indices) {
    verts_indices = _verts_indices;
    position_ws = vec3(0,0,0);
    normal_ws =  vec3(0,0,0);
    verts_ws = {};

}

Face::~Face() {
    //cout << "face deleted!" << endl;
}

Face::Face(const Face &f)
{
    verts_ws = f.verts_ws;
    verts_indices = f.verts_indices;
    position_ws = f.position_ws;
    normal_ws = f.normal_ws;
    id = f.id;
}
        
int Face::setData(const vector<vec3> &verts) {
  verts_ws = {};
  position_ws = vec3(0,0,0);
  for (unsigned int i = 0; i < verts_indices.size(); i++) {
    verts_ws.push_back(verts.at(verts_indices.at(i)));
  }
  for (unsigned int i = 0; i < verts_ws.size(); i++) {
      position_ws += verts_ws.at(i);
  }
  position_ws = position_ws * (1.0f / (float)verts_ws.size());
  if (verts_ws.size() >= 3) {
    normal_ws = normalize(cross(verts_ws.at(0) - verts_ws.at(2), verts_ws.at(1) - verts_ws.at(0)));
  } 
  else {
    normal_ws = vec3(0,0,0);
  }  
  return 0;
}

Edge::Edge(const vector<uint> &verts_indices, const vector<uint> &_faces_indices) {
        v1_index = verts_indices.at(0);
        v2_index = verts_indices.at(1);
        faces_indices = _faces_indices;
}

Edge::~Edge() {
    //cout << "edge deleted!" << endl;
}

int Edge::setData(const vector<vec3> &verts, const vector<Face> &_faces) {
    faces = {_faces.at(faces_indices.at(0)), _faces.at(faces_indices.at(1))};
    v1_ws = verts.at(v1_index);
    v2_ws = verts.at(v2_index);
    return 0;
}

int initTriangle(CollisionBody *body, vector<vec3> verts) {
    CollisionBody::Counter += 1;
    body->id = CollisionBody::Counter;
    body->type = BodyType::TRIANGLE;
    body->inverse_body_inertia_tensor = mat3(vec3(0,0,0), vec3(0,0,0), vec3(0,0,0));
    body->inverse_world_inertia_tensor = mat3(vec3(0,0,0), vec3(0,0,0), vec3(0,0,0));
    body->dX2 = 1.0f;
    body->dY2 = 1.0f;
    body->dZ2 = 1.0f;
    body->faces = {};
    body->edges = {};
    body->coefficient_of_restitution = 0.0f;
    vec3 normal = cross(verts.at(1)-verts.at(0),verts.at(2)-verts.at(1));
    float depth = 0.01f;
    body->a_body_bounding_vertices = verts;
    for (uint i = 0; i < 3; i++) {
        body->a_body_bounding_vertices.push_back(verts.at(i) - normal * depth);
    }
    body->a_bounding_vertices =  body->a_body_bounding_vertices;

    body->mass = FLT_MAX;
    body->one_over_mass = 0.0f;
    body->inverse_body_inertia_tensor = mat4(0.0f);

    vector<vector<uint> > vertex_indices = {{0,1,2}, {3,5,4},{0,3,4,1}, {1,4,5,2}, {5,3,0,2}};
    for (uint i = 0; i < 5; i++) {
        Face f(vertex_indices.at(i));
        f.id = i;
        body->faces.push_back(f);
    }

    vector<vector<uint> > verts_indices = {{0,1},{1,2},{2,0}, {3,4},{4,5},{5,3},{0,3},{1,4},{2,5}};
    vector<vector<uint> > faces_indices = {{0,2},{0,3},{0,4}, {1,2},{1,3},{1,4},{4,2},{2,3},{3,4}};

    for (uint i = 0; i < 9; i++) {
        Edge e(verts_indices.at(i), faces_indices.at(i));
        e.id = i;
        body->edges.push_back(e);
    }
    body->update();

    // calculate cm_position from vertices;
    body->cm_position = vec3(0,0,0);//(body->a_bounding_vertices[0] + body->a_bounding_vertices[1] + body->a_bounding_vertices[2]) / 3.0f;

    /// @todo fix the bounding_radius
    float d1 = distance(body->a_bounding_vertices[0], body->a_bounding_vertices[1]);
    float d2 = distance(body->a_bounding_vertices[1], body->a_bounding_vertices[2]);
    float d3 = distance(body->a_bounding_vertices[0], body->a_bounding_vertices[2]);
    float d = max(d1, max(d2,d3));
    body->bounding_radius = d;
    CollisionBody::allBodies.push_back(body);

    return 0;
}

int initBox(CollisionBody *body, float x, float y, float z) {
    CollisionBody::Counter += 1;
    body->id = CollisionBody::Counter;
    body->type = BodyType::BOX;

    body->dX2 = x * 0.5f;
    body->dY2 = y * 0.5f;
    body->dZ2 = z * 0.5f;

    body->bounding_radius = 0.5f * sqrt(x*x + z*z  + y*y);


    body->mass = body->density * x * y * z;

    body->one_over_mass = 1.0f / body->mass;

    body->inverse_body_inertia_tensor = mat3(vec3(3.0f/(body->mass*(body->dY2*body->dY2+body->dZ2*body->dZ2)),0,0),
                                    vec3(0,3.0f/(body->mass*(body->dX2*body->dX2+body->dZ2*body->dZ2)),0),
                                    vec3(0,0,3.0f/(body->mass*(body->dX2*body->dX2+body->dY2*body->dY2))));

    body->inverse_world_inertia_tensor = mat3(vec3(0,0,0), vec3(0,0,0), vec3(0,0,0));

    body->a_body_bounding_vertices ={vec3(-body->dX2, body->dY2, body->dZ2),
                                    vec3(-body->dX2,-body->dY2, body->dZ2),
                                    vec3( body->dX2,-body->dY2, body->dZ2),
                                    vec3( body->dX2, body->dY2, body->dZ2),
                                    vec3(-body->dX2, body->dY2,-body->dZ2),
                                    vec3(-body->dX2,-body->dY2,-body->dZ2),
                                    vec3( body->dX2,-body->dY2,-body->dZ2),
                                    vec3( body->dX2, body->dY2,-body->dZ2)};

    body->calculateVertices();
    vector<vector<uint> > vertex_indices = {{0,1,2,3}, {4,5,1,0}, {1,5,6,2}, {3,2,6,7}, {4,0,3,7}, {7,6,5,4}};
    for (uint i = 0; i < 6; i++) {
        Face f(vertex_indices.at(i));
        f.id = i;
        body->faces.push_back(f);
    }

    vector<vector<uint> > verts_indices = {{0,1},{1,2},{2,3},{3,0},{5,4},{6,5},{7,6},{4,7},{4,0},{1,5},{6,2},{3,7}};
    vector<vector<uint> > faces_indices = {{0,1},{0,2},{0,3},{0,4},{1,5},{2,5},{3,5},{4,5},{1,4},{1,2},{2,3},{3,4}};

    for (uint i = 0; i < 12; i++) {
        Edge e(verts_indices.at(i), faces_indices.at(i));
        e.id = i;
        body->edges.push_back(e);
    }

    // calculate cm_position from vertices
    body->cm_position = vec3(0,0,0);

    CollisionBody::allBodies.push_back(body);

    return 0;
}

int initSphere(CollisionBody *body, float r) {
    CollisionBody::Counter += 1;
    body->id = CollisionBody::Counter;
    body->dX2 = r;
    body->dY2 = r;
    body->dZ2 = r;
    body->type = BodyType::SPHERE;

    body->bounding_radius = r;
    
    body->a_body_bounding_vertices ={vec3(-body->dX2, body->dY2, body->dZ2),
                                    vec3(-body->dX2,-body->dY2, body->dZ2),
                                    vec3( body->dX2,-body->dY2, body->dZ2),
                                    vec3( body->dX2, body->dY2, body->dZ2),
                                    vec3(-body->dX2, body->dY2,-body->dZ2),
                                    vec3(-body->dX2,-body->dY2,-body->dZ2),
                                    vec3( body->dX2,-body->dY2,-body->dZ2),
                                    vec3( body->dX2, body->dY2,-body->dZ2)};
    body->faces = {};
    body->edges = {};
    body->density = 1.0f;
    body->mass = body->density*(4.0f/3.0f)*pi*body->dX2*body->dY2*body->dZ2;

    body->one_over_mass = 1.0f / body->mass;
    body->inverse_body_inertia_tensor = mat3(vec3(5.0f*body->mass*(body->dY2 * body->dY2 + body->dZ2 * body->dZ2),0,0),
                                       vec3(0,5.0f*body->mass*(body->dX2 * body->dX2 + body->dY2 * body->dY2),0),
                                       vec3(0,0,5.0f*body->mass*(body->dY2 * body->dY2 + body->dY2 * body->dY2)));
    body->coefficient_of_restitution = 1.0f;
    CollisionBody::allBodies.push_back(body);

    return 0;
}

int initCylinder(CollisionBody *body, float r1, float r2, float h) {
    CollisionBody::Counter += 1;
    body->id = CollisionBody::Counter;
    body->dX2 = r1;
    body->dZ2 = r2;
    body->dY2 = h * 0.5f;
    body->type = BodyType::CYLINDER;
    body->density = 1.0f;
    body->mass =  body->density * pi * body->dX2 * body->dZ2 * h;
    body->one_over_mass = 1.0f / body->mass;
    body->inverse_body_inertia_tensor = mat3(vec3(12.0f/(body->mass*(3*r1*r2 + h*h)),0,0),
                                            vec3(0,2.0f/(body->mass*(r1*r2)),0),
                                            vec3(0,0,12.0f/(body->mass*(3*r1*r2 + h*h))));
    body->faces = {};
    body->edges = {};

    vector<vec3> b = {};
    unsigned int resolution = 16;
    for (unsigned int i = 0; i < resolution; i++) {
        b.push_back(vec3(-cos(radians(i/(float)resolution*360.0f))*r1,  body->dY2, sin(radians(i/(float)resolution*360.0f))*r2));
    }
    for (unsigned int i = 0; i < resolution; i++) {
        b.push_back(vec3(-cos(radians(i/(float)resolution*360.0f))*r1, -body->dY2, sin(radians(i/(float)resolution*360.0f))*r2));
    }
    body->bounding_radius = distance(body->cm_position, b.at(0));

    body->a_body_bounding_vertices = b;
    body->calculateVertices();

    vector<vector<uint> > verts_indices = {{ 0, 1},{ 1, 2},{ 2, 3},{ 3, 4},{ 4, 5},{ 5, 6},{ 6, 7},{ 7, 8},{ 8, 9},{ 9,10},{10,11},{11,12},{12,13},{13,14},{14,15},{15, 0},{17,16},{18,17},{19,18},{20,19},{21,20},{22,21},{23,22},{24,23},{25,24},{26,25},{27,26},{28,27},{29,28},{30,29},{31,30},{16,31},{ 0,16},{ 1,17},{ 2,18},{ 3,19},{ 4,20},{ 5,21},{ 6,22},{ 7,23},{ 8,24},{ 9,25},{10,26},{11,27},{12,28},{13,29},{14,30},{15,31}};
    vector<vector<uint> > faces_indices = {{ 0, 1},{ 0, 2},{ 0, 3},{ 0, 4},{ 0, 5},{ 0, 6},{ 0, 7},{ 0, 8},{ 0, 9},{ 0,10},{ 0,11},{ 0,12},{ 0,13},{ 0,14},{ 0,15},{ 0,16},{ 1,17},{ 2,17},{ 3,17},{ 4,17},{ 5,17},{ 6,17},{ 7,17},{8,17},{ 9,17},{10,17},{11,17},{12,17},{13,17},{14,17},{15,17},{16,17},{ 0, 1},{ 1, 2},{ 2, 3},{ 3, 4},{ 4, 5},{ 5, 6},{ 6, 7},{ 7, 8},{ 8, 9},{ 9,10},{10,11},{11,12},{12,13},{13,14},{14,15},{15,16}};

    for (uint i = 0; i < 48; i++) {
        Edge e(verts_indices.at(i), faces_indices.at(i));
        e.id = i;
        body->edges.push_back(e);
    }

    vector<vector<uint> > vertex_indices = {{0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15}, {0, 16, 17, 1}, {1, 17, 18, 2}, {2, 18, 19, 3}, {3, 19, 20, 4},{4, 20, 21, 5},{5, 21, 22, 6},{6, 22, 23, 7},{7, 23, 24, 8},{8, 24, 25, 9},{9, 25, 26, 10},{10, 26, 27, 11},{11, 27, 28, 12},{12, 28, 29, 13},{13, 29, 30, 14},{14, 30, 31, 15},{15, 31, 16, 0},{31, 30, 29, 28, 27, 26, 25, 24, 23, 22, 21, 20, 19, 18, 17, 16}};

    for (uint i = 0; i < 18; i++) {
        Face f(vertex_indices.at(i));
        f.id = i;
        body->faces.push_back(f);
    }

    // calculate cm_position from vertices
    body->cm_position = vec3(0,0,0);

    //body->offset = rotate(mat4(1.0f), radians(90.0f), vec3(1,0,0));
    CollisionBody::allBodies.push_back(body);

    return 0;
}

int RigidBody::Counter = -1;
vector<RigidBody*> RigidBody::allBodies = {};

RigidBody::RigidBody() {
    coefficient_of_restitution = 0.7f;
    density = 0.4f;
    cm_position = vec3(0,0,0);
    orientation = mat3(vec3(1,0,0), vec3(0,1,0), vec3(0,0,1));
    cm_velocity = vec3(0,0,0);
    angular_momentum = vec3(0,0,0);
    cm_force = vec3(0,0,0);
    torque = vec3(0,0,0);
    inverse_world_inertia_tensor = mat3(vec3(1,0,0), vec3(0,1,0), vec3(0,0,1));
    inverse_body_inertia_tensor = mat3(vec3(1,0,0), vec3(0,1,0), vec3(0,0,1));
    angular_velocity = vec3(0,0,0);
    mass = 1.0f;
    one_over_mass = 1.0f / mass; 
    RigidBody::Counter += 1;
    id = RigidBody::Counter;
    RigidBody::allBodies.push_back(this);
};

RigidBody::~RigidBody() {
}

mat4 RigidBody::getModelMatrix() {
    mat4 m = translate(mat4(1.0f), cm_position) * mat4(orientation);
    //m = scale(m, vec3(dX2, dY2, dZ2));
    //m = m;
    return m;
}

void RigidBody::add(int bodyID) {
    int bodyIndex = CollisionBody::getIndex(bodyID);
    CollisionBody *collision_body = CollisionBody::allBodies.at(bodyIndex);

    collision_body->compositeBodyIndex = id;

    bodyIndexes.push_back(bodyIndex);

    // Single body
    if (bodyIndexes.size() == 1) {
        mass = collision_body->mass;
        cm_position = collision_body->cm_position;
        one_over_mass = 1.0f / mass;
        orientation = collision_body->orientation;
        inverse_body_inertia_tensor = collision_body->inverse_body_inertia_tensor;
        type = collision_body->type;

    // Multi body
    } else {
        // recalculate mass data
        // restore cm_position to before the divide by mass, update the numerator, then divide through by updated mass
        cm_position *= mass;
        mass += collision_body->mass;
        cm_position += collision_body->cm_position * collision_body->mass;
        one_over_mass = 1.0f / mass;
        cm_position *= one_over_mass;

        if (all(isnan(cm_position))) {
            cm_position = vec3(0,0,0);
        }

        mat4 model = translate(mat4(1.0f), collision_body->cm_position) * mat4(collision_body->orientation);
        mat4 composite = translate(mat4(1.0f), cm_position) * mat4(orientation);
        // calculate the offset in composite body space
        // eg. if composite body is at vec3(1,0,0) and body is at vec3(2,0,0)
        // offset would be vec3(1,0,0) (obviously in matrix form)
        // where composite * offset = model
        collision_body->offset =  inverse(composite) * model;

        mat3 Ib = orientation * inverse(collision_body->inverse_body_inertia_tensor) * inverse(orientation);

        // move COM from body_b to composite body COM ( TO DO check this works properly, ie. shouldnt it be -=?)
        Ib += parallelAxis(getPos(collision_body->offset)) * collision_body->mass;
        // uninvert, accumulate at composite body origin, and invert again
        inverse_body_inertia_tensor = inverse(inverse_body_inertia_tensor);
        inverse_body_inertia_tensor += Ib;
        inverse_body_inertia_tensor = inverse(inverse_body_inertia_tensor);

        bool b0 = all(isnan(inverse_body_inertia_tensor[0]));
        bool b1 = all(isnan(inverse_body_inertia_tensor[1]));
        bool b2 = all(isnan(inverse_body_inertia_tensor[2]));

        if (b0 && b1 && b2) {
            inverse_body_inertia_tensor = mat3(0.0f);
        }

        type = BodyType::COMPOSITE;
    }

    for (unsigned int i = 0; i < bodyIndexes.size(); i++) {
        unsigned int ind = bodyIndexes.at(i);
        CollisionBody *collision_body = CollisionBody::allBodies.at(bodyIndex);
        mat4 model = translate(mat4(1.0f), collision_body->cm_position) * mat4(collision_body->orientation);
        mat4 composite = translate(mat4(1.0f), cm_position) * mat4(orientation);
        // calculate the offset in composite body space
        // eg. if composite body is at vec3(1,0,0) and body is at vec3(2,0,0)
        // offset would be vec3(1,0,0) (obviously in matrix form)
        // where composite * offset = model
        collision_body->offset =  inverse(composite) * model;
    }
}

bool RigidBody::getColliding() {
    for (unsigned int i = 0; i < bodyIndexes.size(); i++) {
        unsigned int ind = bodyIndexes.at(i);
        CollisionBody *collision_body = CollisionBody::allBodies.at(ind);
        if (collision_body->colliding) {
            return true;
        }
    }
    return false;
}

int RigidBody::integrateForces(vec3 gravity, float deltaTime) {
    if (one_over_mass == 0.0f) {
        cm_force = vec3(0,0,0);
    } else {
        cm_force = gravity;
    }
    // integrate primary quantities
    cm_velocity += cm_force * one_over_mass * deltaTime;
    // compute auxillary quantities
    inverse_world_inertia_tensor = orientation * inverse_body_inertia_tensor * inverse(orientation);
    vec3 gyroscopic_term;
    if (apply_gyroscopic_term) {
        gyroscopic_term = angular_velocity * inverse(inverse_world_inertia_tensor) * skewSymmetric(angular_velocity);
    } else {
        gyroscopic_term = vec3(0,0,0);
    }
    angular_momentum += (torque - gyroscopic_term) * deltaTime; 
    angular_velocity = inverse_world_inertia_tensor * angular_momentum;
    return 0;
}

int RigidBody::integrateVelocities(float deltaTime) {
    // integrate primary quantities
    cm_position += cm_velocity * deltaTime;
    orientation += skewSymmetric(angular_velocity) * orientation * deltaTime;

    orientation = orthonormalizeOrientation(orientation);
    torque = vec3(0,0,0);
    cm_force = vec3(0,0,0);

    return 0;
}
