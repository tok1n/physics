#include "collision.hpp"

// prefer face-face contacts over edge-edge contacts
float edgeRelativeTolerance = 0.85f;
float edgeAbsoluteTolerance = 0.03f;
float faceRelativeTolerance = 0.81f;
float faceAbsoluteTolerance = 0.03f;

float depth_epsilon = 0.001f; 
  
// find furthest along n
unsigned int getSupport(CollisionBody &hull, vec3 n) {
    vec3 _v;
    float _d = -FLT_MAX;
    unsigned int _i = -1;

    for (unsigned int i = 0; i < hull.a_bounding_vertices.size(); i++) {
        vec3 v = hull.a_bounding_vertices.at(i);

        float d = dot(v, n);
        if (_d == -FLT_MAX || d > _d) {
            _d = d;
            _v = v;
            _i = i;
        }
    }
    return _i;
}

bool sphereSphereCollision(const CollisionBody &hullA, const CollisionBody &hullB) {
    return distance(hullA.cm_position, hullB.cm_position) < (hullA.dX2 + hullB.dX2);
}


bool sphereBoxCollision(const CollisionBody &hullA, vec3 closest_pt) {
    return distance(hullA.cm_position, closest_pt) - hullA.dX2 < 0.0f;
}
vec3 closestPointOnCylinder(vec3 p, const CollisionBody &cylinder) {
    vec3 p1 = cylinder.cm_position - cylinder.orientation[1];
    vec3 p2 = cylinder.cm_position + cylinder.orientation[1];
    float dist = dot(p - p1, p2 - p1);
    float cylinder_r = cylinder.dX2;
    vec3 point;
    bool above_top = pointPlaneDistance(p, p2, cylinder.orientation[1]) > 0.0f;
    bool below_bottom = pointPlaneDistance(p, p1, -cylinder.orientation[1]) > 0.0f;

    // above top of cylinder
    if (above_top) {
        point = closestPointOnCircle(p, p2, normalize(cylinder.orientation[1]), cylinder_r);

    // between top and bottom
    } else if ( !below_bottom && !above_top ) {
        float d = pointLineIntersection(p, p1, p2);
        vec3 point_on_line_segment = p1 + (p2 - p1) * d;
        point = point_on_line_segment + normalize(p - point_on_line_segment) * cylinder_r;

    // below bottom of cylinder
    } else {
        point = closestPointOnCircle(p, p1, normalize(cylinder.orientation[1]) * -1.0f, cylinder_r);
    }
    return point;
}

bool sphereCylinderCollision(const CollisionBody &body_a, vec3 closest_pt) {
    return distance(body_a.cm_position, closest_pt) - body_a.dX2 < 0.0f;
}

// initially assume unit cube for easier testing
bool cylinderBoxCollision(const CollisionBody &cyl, const CollisionBody &obb) {

    return 0;
}

Query queryFaceDirections(CollisionBody &hullA, CollisionBody &hullB) {
    float best_distance = -FLT_MAX;
    ivec2 collision_index(-1,-1);
    vec3 normal(0,0,0);
    // @todo *try* not to test against bottom face of triangle
    unsigned int range = hullA.faces.size();
    // if (hullB.type == BodyType::TRIANGLE) {
    //     range = 1;
    // }
    for (unsigned i = 0; i < range; i++) {
        Face *f = &hullA.faces.at(i);
        unsigned int vertexIndex = getSupport(hullB, f->normal_ws * -1.0f);

        vec3 vertexB = hullB.a_bounding_vertices.at(vertexIndex);

        float dist = pointPlaneDistance(vertexB, f->position_ws, f->normal_ws);
        if (dist > best_distance) {
            collision_index = ivec2(i, -1);
            best_distance = dist;
            normal = f->normal_ws;
        }
    }
    return Query(best_distance, collision_index, normal);
}

Query queryEdgeDirections(CollisionBody &hullA, CollisionBody &hullB) {
    float best_distance = -FLT_MAX;
    ivec2 collision_index = ivec2(-1,-1);
    vec3 normal;

    for (unsigned i = 0; i < hullA.edges.size(); i++) {
        Edge *edge_a = &hullA.edges.at(i);
        for (unsigned j = 0; j < hullB.edges.size(); j++) {
            Edge *edge_b = &hullB.edges.at(j);

            // negate last two values for minkowski difference
            vec3 edge_a_n1 = edge_a->faces.at(0).normal_ws;
            vec3 edge_a_n2 = edge_a->faces.at(1).normal_ws;
            vec3 edge_b_n1 = edge_b->faces.at(0).normal_ws;
            vec3 edge_b_n2 = edge_b->faces.at(1).normal_ws;

            // prune edge pairs that don't build a Minkowski face, since they can never collide
            bool builds_face = buildMinkowskiFace(edge_a_n1,         edge_a_n2,         cross(edge_a_n2, edge_a_n1) * -1.0f,
                                                  edge_b_n1 * -1.0f, edge_b_n2 * -1.0f, cross(edge_b_n2,edge_b_n1) * -1.0f);
            if (!builds_face) {
                continue;
            }
            vec3 axis = normalize(cross(edge_a->v2_ws - edge_a->v1_ws, edge_b->v2_ws - edge_b->v1_ws));

            // ignore if edges are close to parallel
            if (length(axis) < 0.005f) {
                continue;
            }
            // make sure normal is pointing away from A
            if (dot(axis, edge_a->v1_ws - hullA.cm_position) < 0.0f) {
                axis = axis * -1.0f;
            }

            // check if edge segments are intersecting
            vec2 mu = lineSegmentIntersection(edge_a->v1_ws, edge_a->v2_ws, edge_b->v1_ws, edge_b->v2_ws);
            float mu_a = mu.x;
            float mu_b = mu.y;
            bool valid_intersection = mu_a >= 0.0f && mu_a <= 1.0f && mu_b >= 0.0f && mu_b <= 1.0f;

            // keep largest penetration
            if (valid_intersection) {
                vec3 pa = edge_a->v1_ws + (edge_a->v2_ws - edge_a->v1_ws) * mu_a;
                vec3 pb = edge_b->v1_ws + (edge_b->v2_ws - edge_b->v1_ws) * mu_b;

                float penetration = dot(pb - pa, axis);
                if (penetration > best_distance) {
                    best_distance = penetration;
                    collision_index = ivec2(edge_a->id, edge_b->id);
                    normal = axis; 
                }
            }
        }
    }
    return Query(best_distance, collision_index, normal);
}

bool SAT(CollisionBody &hullA, CollisionBody &hullB, Query &query) {

    // test all normals of hullA as axes
    Query face_query_a = queryFaceDirections(hullA, hullB);

    face_query_a.id = QueryType::FACE_AB;
    if (face_query_a.best_distance > 0.0f) {
        return false;
    }

    // and all normals of hullB as axes
    Query face_query_b = queryFaceDirections(hullB, hullA);
    face_query_b.id = QueryType::FACE_BA;
    if (face_query_b.best_distance > 0.0f) {
        return false;
    }

    // and the cross product of the edges
    Query edge_query = queryEdgeDirections(hullA, hullB);
    edge_query.id = QueryType::EDGE_AB;

    // no collision
    if (edge_query.best_distance > 0.0f) {
        return false;
    }   

    // collision
    // impose artificial bias to favour face collisions for better coherence
    float edge_best_distance = edge_query.best_distance * edgeRelativeTolerance;
    bool face_contact_a = face_query_a.best_distance + edgeAbsoluteTolerance > edge_best_distance;
    bool face_contact_b = face_query_b.best_distance + edgeAbsoluteTolerance > edge_best_distance;

    bool _face_contact_a = face_query_a.best_distance > edge_best_distance;
    bool _face_contact_b = face_query_b.best_distance > edge_best_distance;

    bool face_contact_a_over_face_contact_b = face_query_a.best_distance + faceAbsoluteTolerance > face_query_b.best_distance * faceRelativeTolerance;
    bool _face_contact_a_over_face_contact_b = face_query_a.best_distance > face_query_b.best_distance;

    if (face_contact_a && face_contact_b) {
        if (face_contact_a_over_face_contact_b) {
            if (!_face_contact_a_over_face_contact_b) {
                //std::cout << "would have been face_contact_b" << std::endl;
            }
            query = face_query_a;
            return true;
        } else {
            query = face_query_b;
            return true;
        }
    } else {
        if (face_contact_a) {
            if (!_face_contact_a) {
                //std::cout << "would have been edge_contact" << std::endl;
            }
            query = face_query_a;
            return true;
        } else if (face_contact_b) {
            if (!_face_contact_b) {
                //std::cout << "would have been edge_contact" << std::endl;
            }
            query = face_query_b;
            return true;
        } else {
            query = edge_query;
            return true;
        }
    }
}