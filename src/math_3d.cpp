#include "math_3d.hpp"

vec3 findOrthogonal(vec3 v) {
    if (fabs(v.x) >=  1.0f / sqrt(3.0f)) {
        return normalize(vec3(v.y, -v.x, 0.0f));
    } else {
        return normalize(vec3(0.0f, v.z, -v.y));
    }
}

mat3 formOrthogonalBasis(vec3 const &v) {
    vec3 a = findOrthogonal(v);
    vec3 b = cross(a,v);
    return mat3(v,a,b);
}

mat3 orthonormalizeOrientation(mat3 m) {
    m[0] = normalize(m[0]);
    m[2] = normalize(cross(m[0], m[1]));
    m[1] = normalize(cross(m[2], m[0]));
    return m;
}

mat3 skewSymmetric(vec3 const &vector) {
    return mat3(vec3(      0.0,  vector.z, -vector.y),
                vec3(-vector.z,         0,  vector.x),
                vec3( vector.y, -vector.x,        0));
}

vector<vec3> getAABBVertices(const vec3 &_min, const vec3 &_max) {
    vec3 point1 = vec3(_min.x, _min.y, _min.z);
    vec3 point2 = vec3(_max.x, _max.y, _max.z);
    vec3 point3 = vec3(point1.x, point2.y, point1.z);
    vec3 point4 = vec3(point2.x, point1.y, point1.z);
    vec3 point5 = vec3(point2.x, point2.y, point1.z);
    vec3 point6 = vec3(point2.x, point1.y, point2.z);
    vec3 point7 = vec3(point1.x, point2.y, point2.z);
    vec3 point8 = vec3(point1.x, point1.y, point2.z);
    vector<vec3> v = {point8, point6, point2, point7, point1, point4, point5, point3};
    return v;
}

float clamp(float value, float _min, float _max) {
    return std::min(std::max(value, _min), _max);
}

vector<float> getAABB(const vector<vec3> &vertices) {
    float minX = FLT_MAX;
    float maxX = -FLT_MAX;
    float minY = FLT_MAX;
    float maxY = -FLT_MAX;
    float minZ = FLT_MAX;
    float maxZ = -FLT_MAX;

    for (unsigned int i = 0; i < vertices.size(); i++) {
        if (vertices.at(i).x < minX) {
            minX = vertices.at(i).x;
        }
        else if (vertices.at(i).x > maxX) {
            maxX = vertices.at(i).x;
        }
        if (vertices.at(i).y < minY) {
            minY = vertices.at(i).y;
        }
        else if (vertices.at(i).y > maxY) {
            maxY = vertices.at(i).y;
        }
        if (vertices.at(i).z < minZ) {
            minZ = vertices.at(i).z;
        }
        else if (vertices.at(i).z > maxZ) {
            maxZ = vertices.at(i).z;
        }
    }
    vector<float> limits = {minX, minY, minZ, maxX, maxY, maxZ};
    return limits;
}

bool AABBOverlap(const vec3 &_min1, const vec3 &_max1, const vec3 &_min2, const vec3 &_max2) {
    return _max2.x > _min1.x && _max1.x > _min2.x &&
           _max2.y > _min1.y && _max1.y > _min2.y &&
           _max2.z > _min1.z && _max1.z > _min2.z;
}

float distPointToTriangle(const vec3 &point, const vec3 &v1, const vec3 &v2, const vec3 &v3) {
    vec3 n = normalize(cross(v2 - v1, v3 - v1));
    // 1. project point onto plane of triangle
    vec3 projected_point = point - n * dot(point, n);
    // 2. inside-outside test for each edge
    vec3 e1 = v2 - v1;
    vec3 e2 = v3 - v2;
    vec3 e3 = v1 - v3;
    float min_dist = -FLT_MAX;
    vec3 c1 = projected_point - v1; 
    vec3 c2 = projected_point - v2;
    vec3 c3 = projected_point - v3;
    float d1 = dot(n, cross(e1, c1));
    if (d1 < 0) {
        // outside of edge 1
        if (d1 > min_dist) {
            min_dist = d1;
        }
    }
    float d2 = dot(n, cross(e2, c2));
    if (d2 < 0) {
        // outside of edge 2
        if (d2 > min_dist) {
            min_dist = d2;
        }
    }
    float d3 = dot(n, cross(e3, c3));
    if (d3 < 0) {
        // outside of edge 3
        if (d3 > min_dist) {
            min_dist = d3;
        }
    }

    return min_dist;
}

vec3 matrixMultiply(const mat4 &m, const vec3 &v) {
    vec4 v4 = vec4(v.x, v.y, v.z, 1.0f);
    vec3 v1 = m * v4;
    return vec3(v1.x, v1.y, v1.z);
}

float pointLineIntersection(const vec3 &p3, const vec3 &p1, const vec3 &p2) {

    float x1 = p1.x;
    float y1 = p1.y;
    float z1 = p1.z;
    float x2 = p2.x;
    float y2 = p2.y;
    float z2 = p2.z;
    float x3 = p3.x;
    float y3 = p3.y;
    float z3 = p3.z;

    float n = (x3-x1)*(x2-x1) + (y3-y1)*(y2-y1) + (z3-z1)*(z2-z1);
    float d = pow(length(p2 - p1),2);
    if (d == 0) {
        return numeric_limits<float>::infinity();
    }
    float u =  n / d;
    return u; // p = p1 + u*(p2-p1)
}

vec2 lineSegmentIntersection(const vec3 &p1, const vec3 &p2, const vec3 &p3, const vec3 &p4) {
    float d1343 = dot(p1 - p3, p4 - p3);
    float d4321 = dot(p4 - p3, p2 - p1);
    float d1321 = dot(p1 - p3, p2 - p1);
    float d4343 = dot(p4 - p3, p4 - p3);
    float d2121 = dot(p2 - p1, p2 - p1);
    float mu_a = (d1343*d4321-d1321*d4343)/(d2121*d4343-d4321*d4321);
    float mu_b = (d1343 + mu_a*d4321) / d4343;
    return vec2(mu_a, mu_b);
}

vec2 closestPointsOnLineSegments(
    const vec3 &P0, const vec3 &P1,
    const vec3 &Q0, const vec3 &Q1)
{
    float const SMALL_NUM = 0.00000001f;
    vec3   u = P1 - P0;
    vec3   v = Q1 - Q0;
    vec3   w = P0 - Q0;
    float    a = dot(u, u);         // always >= 0
    float    b = dot(u, v);
    float    c = dot(v, v);         // always >= 0
    float    d = dot(u, w);
    float    e = dot(v, w);
    float    D = a*c - b*b;        // always >= 0
    float    sc, sN, sD = D;       // sc = sN / sD, default sD = D >= 0
    float    tc, tN, tD = D;       // tc = tN / tD, default tD = D >= 0

    // compute the line parameters of the two closest points
    if (D < SMALL_NUM) { // the lines are almost parallel
        sN = 0.0f;         // force using point P0 on segment S1
        sD = 1.0f;         // to prevent possible division by 0.0 later
        tN = e;
        tD = c;
    }
    else {                 // get the closest points on the infinite lines
        sN = (b*e - c*d);
        tN = (a*e - b*d);
        if (sN < 0.0f) {        // sc < 0 => the s=0 edge is visible
            sN = 0.0f;
            tN = e;
            tD = c;
        }
        else if (sN > sD) {  // sc > 1  => the s=1 edge is visible
            sN = sD;
            tN = e + b;
            tD = c;
        }
    }

    if (tN < 0.0f) {            // tc < 0 => the t=0 edge is visible
        tN = 0.0f;
        // recompute sc for this edge
        if (-d < 0.0f)
            sN = 0.0f;
        else if (-d > a)
            sN = sD;
        else {
            sN = -d;
            sD = a;
        }
    }
    else if (tN > tD) {      // tc > 1  => the t=1 edge is visible
        tN = tD;
        // recompute sc for this edge
        if ((-d + b) < 0.0f)
            sN = 0;
        else if ((-d + b) > a)
            sN = sD;
        else {
            sN = (-d + b);
            sD = a;
        }
    }
    // finally do the division to get sc and tc
    sc = (fabs(sN) < SMALL_NUM ? 0.0f : sN / sD);
    tc = (fabs(tN) < SMALL_NUM ? 0.0f : tN / tD);

    // get the difference of the two closest points
    float s = sc;
    float t = tc;
    return vec2(s,t);
}


float pointPlaneDistance(const vec3 &point, const vec3 &planePosition, const vec3 &planeNormal) {
    return dot(point - planePosition, planeNormal);
}

vec3 pointLineSegmentIntersection(vec3 p, vec3 l1, vec3 l2) {
    vec3 n = normalize(l2 - l1);
    // compute u and v
    float v = dot(p - l1, n);
    float u = dot(l2 - p, n);
    if (u <= 0.0f) {
        return l2;
    } else if (v <= 0.0f) {
        return l1;
    }
    else {
        return l1 + n * v;
    }
}

vec3 pointTriangleIntersection(vec3 pt, vec3 t1, vec3 t2, vec3 t3, unsigned int &type) {
    vec3 n = normalize(cross(t2 - t1, t3 - t2));

    // test if point is "inside" triangle
    float s1 = dot(cross(t2 - t1, pt - t1), n);
    float s2 = dot(cross(t3 - t2, pt - t2), n);
    float s3 = dot(cross(t1 - t3, pt - t3), n);

    if (s1 >= 0.0f && s2 >= 0.0f && s3 >= 0.0f) {
        // project point onto the plane
        float d = pointPlaneDistance(pt, t1, n);
        // FACE = 0
        type = 0;
        return pt - n * d;
    }

    // find closest point on an edge
    else {
        vec3 q1 = pointLineSegmentIntersection(pt, t1, t2);
        vec3 q2 = pointLineSegmentIntersection(pt, t2, t3);
        vec3 q3 = pointLineSegmentIntersection(pt, t3, t1);

        float d1 = distance(pt,q1);
        float d2 = distance(pt,q2);
        float d3 = distance(pt,q3);
        float l = std::min(d1, std::min(d2, d3));
        if (l == d1) {
            type = 1;
            return q1;
        } else if (l == d2) {
            type = 2;
            return q2;
        } else {
            type = 3;
            return q3;
        }
    }
}

vec3 pointQuadIntersection(vec3 pt, vec3 t1, vec3 t2, vec3 t3, vec3 t4) {
    vec3 n = normalize(cross(t2 - t1, t3 - t2));

    // test if point projected onto the plane is inside quad
    float s1 = dot(cross(t2 - t1, pt - t1), n);
    float s2 = dot(cross(t3 - t2, pt - t2), n);
    float s3 = dot(cross(t4 - t3, pt - t3), n);
    float s4 = dot(cross(t1 - t4, pt - t4), n);

    if (s1 >= 0.0f && s2 >= 0.0f && s3 >= 0.0f && s4 >= 0.0f) {
        // project point onto the plane
        float d = pointPlaneDistance(pt, t1, n);
        return pt - n * d;
    }

    // find closest point on an edge
    else {
        vec3 q1 = pointLineSegmentIntersection(pt, t1, t2);
        vec3 q2 = pointLineSegmentIntersection(pt, t2, t3);
        vec3 q3 = pointLineSegmentIntersection(pt, t3, t4);
        vec3 q4 = pointLineSegmentIntersection(pt, t4, t1);

        float d1 = distance(pt,q1);
        float d2 = distance(pt,q2);
        float d3 = distance(pt,q3);
        float d4 = distance(pt,q4);

        float l = std::min(d1, std::min(d2, std::min(d3, d4)));

        if (l == d1) {
            return q1;
        } else if (l == d2) {
            return q2;
        } else if (l == d3) {
            return q3;
        } else {
            return q4;
        }
    }
}

// Sutherland-Hodgeman clipping algorithm 
// (modified: sets contactIDs by keeping the memory layout the same when a new clip point is created
// for which the unique key can then be indexed for contact generation and warm starting)
bool clipPolygonToPlane(vector<vec3> &polygon, const vec3 &planePosition, const vec3 &planeNormal, vector<vector<int> > &contactIDs, unsigned int planeIndex) {
    vector<vec3> out = {};
    vector<vector<int> > outIDs = {};
    bool clipped = false;
    if (polygon.size() == 0) {
        return clipped;
    }
    
    unsigned clipPlaneIDOffset = 8; // @todo figure out how to make this 10 for triangles 11 for others

    vec3 vertex1 = polygon.at(polygon.size() -1);

    float distance1 = pointPlaneDistance(vertex1, planePosition, planeNormal);
    for (unsigned int i = 0; i < polygon.size(); i++) {
        vector<int> currentID = contactIDs.at(i);
        vec3 vertex2 = polygon.at(i);
        float fraction = 0.0f;

        float distance2 = pointPlaneDistance(vertex2, planePosition, planeNormal);
        if (distance1 <= 0.0f && distance2 <= 0.0f) {
            out.push_back(vertex2);
            // nothing changed for this point, leave value for this clip plane at 1
            vector<int> clipPointID = currentID;

            clipPointID.at(clipPlaneIDOffset + planeIndex) = 1;
            outIDs.push_back(clipPointID);

        } else if (distance1 <= 0.0f && distance2 > 0.0f) {
            fraction = distance1 / (distance1 - distance2);
            vec3 intersectionPoint = vertex1 + (vertex2 - vertex1) * fraction;
            out.push_back(intersectionPoint);

            vector<int> clipPointID = currentID;
            // point was clipped against this plane, change value for this clip plane to 2

            clipPointID.at(clipPlaneIDOffset + planeIndex) = 2;

            outIDs.push_back(clipPointID);

            clipped = true;

        } else if (distance2 <= 0.0f && distance1 > 0.0f) {
            fraction = distance1 / (distance1 - distance2);
            vec3 intersectionPoint = vertex1 + (vertex2 - vertex1) * fraction;
            out.push_back(intersectionPoint);

            vector<int> clipPointID = currentID;
            // point was clipped against this plane, change value for this clip plane to 3

            clipPointID.at(clipPlaneIDOffset + planeIndex) = 3;

            outIDs.push_back(clipPointID);

            out.push_back(vertex2);
            // nothing changed for this point, change value for this clip plane at 4 
            clipPointID = currentID;

            clipPointID.at(clipPlaneIDOffset + planeIndex) = 4;

            outIDs.push_back(clipPointID);
            clipped = true;

        }

        // keep vertex2 as starting vertex for next edge
        vertex1 = vertex2;
        distance1 = distance2;
    }
    polygon = out;
    contactIDs = outIDs;
    return clipped;
}

bool buildMinkowskiFace(const vec3 &a, const vec3 &b, const vec3 &b_x_a, const vec3 &c, const vec3 &d, const vec3 &d_x_c) {
    float cba = dot(c, b_x_a);
    float dba = dot(d, b_x_a);
    float adc = dot(a, d_x_c);
    float bdc = dot(b, d_x_c);
    return cba * dba < 0.0f && adc * bdc < 0.0f && cba * bdc > 0.0f;
}

vec3 closestPointOnCircle(vec3 p, vec3 plane_position, vec3 plane_normal, float r) {
    float d = pointPlaneDistance(p, plane_position, plane_normal);
    vec3 closest_point_on_plane = p - plane_normal * d;
    if (distance(closest_point_on_plane, plane_position) < r) {
        return closest_point_on_plane;
    } else {
        return plane_position + normalize(closest_point_on_plane - plane_position) * r;
    }
}

float spherePlaneCollision(vec3 sphereCentre, vec3 planeNormal, vec3 pointOnPlane) {
    vec3 v = sphereCentre - pointOnPlane;
    float d = dot(v, planeNormal);

    // penetration depth along v
    return d;
}

bool rayTriangleCollision(vec3 planeIntersection, vec3 planeNormal, vec3 v1, vec3 v2, vec3 v3) {
    vec3 edge0;
    vec3 edge1;
    vec3 edge2;

    vec3 C0;
    vec3 C1;
    vec3 C2;

    edge0 = v2 - v1; 
    edge1 = v3 - v2;
    edge2 = v1 - v3; 

    C0 = planeIntersection - v1; 
    C1 = planeIntersection - v2;
    C2 = planeIntersection - v3; 

    // check that the vector between each vertex and the plane intersection
    // is left of its corresponding triangle side, ie. the dot product is +ve
    if (dot(planeNormal, cross(edge0, C0)) > 0 && 
        dot(planeNormal, cross(edge1, C1)) > 0 && 
        dot(planeNormal, cross(edge2, C2)) > 0) {
        return true; // plane_intersection is inside the triangle
    }
    return false;
}

vec3 closestPointOnLineSegment(vec3 A, vec3 B, vec3 point) {
    vec3 AB = B - A;
    float t = dot(point - A, AB) / (0.00001f + dot(AB, AB));
    return A + AB * std::min(std::max(t, 0.0f), 1.0f);
}

// Given point p, return point q on (or in) OBB b, closest to p
vec3 closestPointOnOBB(vec3 point, float box_dX2, float box_dY2, float box_dZ2, vec3 box_position, mat3 box_orientation) {
    vector<float> box_extents = {box_dX2, box_dY2, box_dZ2};
    vec3 d = point - box_position;
    // start result at center of box; make steps from there
    vec3 q = box_position;
    // For each OBB axis...
    for (unsigned i = 0; i < box_extents.size(); i++) {
        // ...project d onto that axis to get the distance
        //along the axis of d from the box center
        float dist = dot(d, box_orientation[i]);
        // If distance farther than the box extents, clamp to the box
        if (dist > box_extents.at(i)) {
            dist = box_extents.at(i);
        }
        if (dist < -box_extents.at(i)) {
            dist = -box_extents.at(i);
        }
        // Step that distance along the axis to get world coordinate
        q += box_orientation[i] * dist;
    }
    return q;
}

mat3 parallelAxis(vec3 r) {
    return dot(r,r) * mat3(1.0f) - outerProduct(r, r);
}

vec3 getPos(mat4 m) {
    vec4 v = m[3];
    return vec3(v.x, v.y, v.z);
}
