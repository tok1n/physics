#include "contact.hpp"

#define GLM_ENABLE_EXPERIMENTAL
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/string_cast.hpp>
#include <glm/gtx/euler_angles.hpp>

#include <map>

int warmStartRecord = 0;

vector<Contact> Contact::allContacts = {};
unsigned int Contact::iterations = 15;

extern TriOctree octree;

vector<Contact> getContacts(CollisionBody &body) {
    vector<Contact> current_contacts = {};
    for (unsigned int i = 0; i < Contact::allContacts.size(); i++) {
        Contact *contact = &Contact::allContacts.at(i);
        if (contact->col_body_a->id == body.id) {
            current_contacts.push_back(*contact);
        } else if (contact->col_body_b->id == body.id) {
            current_contacts.push_back(*contact);
        }
    }
    return current_contacts;
}

Contact::Contact(CollisionBody &hullA, CollisionBody &hullB) {
    col_body_a = &hullA;
    col_body_b = &hullB;
    body_a = RigidBody::allBodies.at(hullA.compositeBodyIndex);
    body_b = RigidBody::allBodies.at(hullB.compositeBodyIndex);
}

int Contact::preStep(float dt) {

    vec3 impulse_direction = normalize(normal);
    vec3 pa = collision_point;
    vec3 pb = collision_point;
    vec3 ra = pa - body_a->cm_position;
    vec3 va = body_a->cm_velocity + cross(body_a->angular_velocity, ra);

    vec3 rb = pb - body_b->cm_position;
    vec3 vb = body_b->cm_velocity + cross(body_b->angular_velocity, rb);

    vec3 velocity = vb - va;
    float relative_velocity = dot(velocity, impulse_direction);

    effective_mass_normal =   body_a->one_over_mass + body_b->one_over_mass
                            + dot(cross(ra, impulse_direction), body_a->inverse_world_inertia_tensor * cross(ra, impulse_direction))
                            + dot(cross(rb, impulse_direction), body_b->inverse_world_inertia_tensor * cross(rb, impulse_direction));  
    effective_mass_normal = 1.0f / effective_mass_normal;

    // @todo fix friction curving
    // vec3 vn  = dot(velocity, impulse_direction) * impulse_direction;
    // tangent = normalize(relative_velocity - vn) * -1.0f;
    // if (tangent == vec3(0,0,0)) {
    //     mat3 basis = formOrthogonalBasis(normal);
    //     tangent = basis[1];
    // }

    mat3 basis = formOrthogonalBasis(normal);
    tangent = normalize(basis[1]);

    impulse_direction = tangent;
    ra = pa - body_a->cm_position;
    va = body_a->cm_velocity + cross(body_a->angular_velocity, ra);
    rb = pb - body_b->cm_position;
    vb = body_b->cm_velocity + cross(body_b->angular_velocity, rb);

    effective_mass_tangent =   body_a->one_over_mass + body_b->one_over_mass
                            + dot(cross(ra, impulse_direction), body_a->inverse_world_inertia_tensor * cross(ra, impulse_direction))
                            + dot(cross(rb, impulse_direction), body_b->inverse_world_inertia_tensor * cross(rb, impulse_direction));  
    effective_mass_tangent = 1.0f / effective_mass_tangent;

    // @todo fix friction curving
    // bitangent = normalize(cross(tangent,vn));
    // if (dot(bitangent, velocity) > 0.0f) {
    //     bitangent = bitangent * -1.0f;
    // }

    bitangent = normalize(basis[2]);

    impulse_direction = bitangent;

    ra = pa - body_a->cm_position;
    va = body_a->cm_velocity + cross(body_a->angular_velocity, ra);
    rb = pb - body_b->cm_position;
    vb = body_b->cm_velocity + cross(body_b->angular_velocity, rb);

    effective_mass_bitangent =   body_a->one_over_mass + body_b->one_over_mass
                            + dot(cross(ra, impulse_direction), body_a->inverse_world_inertia_tensor * cross(ra, impulse_direction))
                            + dot(cross(rb, impulse_direction), body_b->inverse_world_inertia_tensor * cross(rb, impulse_direction));  
    effective_mass_bitangent = 1.0f / effective_mass_bitangent;

    // calculate bias term
    float k_min_penetration = 0.01f;
    float k_biasFactor = 0.1f;
    bias = -k_biasFactor * 1.0f / dt * std::min(0.0f, penetration + k_min_penetration);

    //@todo fix restitution
/*    float k_min_velocity = 1.0f;
    float e = (body_a->coefficient_of_restitution + body_b->coefficient_of_restitution) * 0.5f;
    if (relative_velocity < -k_min_velocity) {
        if (e * relative_velocity < bias) {
            bias = -e * relative_velocity;
        }
    }*/
 
    vec3 impulse = normalize(normal) * Pn + normalize(tangent) * Pt + normalize(bitangent) * Pbt;
    // apply impulse to primary quantities;
    body_a->cm_velocity -= impulse * body_a->one_over_mass;
    body_a->angular_momentum -= cross(ra, impulse);
    // compute affected auxillary quantities;
    body_a->angular_velocity = body_a->inverse_world_inertia_tensor * body_a->angular_momentum;
    // apply impulse to primary quantities;
    body_b->cm_velocity += impulse * body_b->one_over_mass;
    body_b->angular_momentum += cross(rb, impulse);
    // compute affected auxillary quantities;
    body_b->angular_velocity = body_b->inverse_world_inertia_tensor * body_b->angular_momentum;

    return 0;
}

int Contact::applyImpulse() {
    vec3 impulse_direction = normal;

    vec3 pa = collision_point;
    vec3 pb = collision_point;
    vec3 ra = pa - body_a->cm_position;
    vec3 va = body_a->cm_velocity + cross(body_a->angular_velocity, ra);
    vec3 rb = pb - body_b->cm_position;
    vec3 vb = body_b->cm_velocity + cross(body_b->angular_velocity, rb);
    vec3 velocity = vb - va;
    float impulse_numerator = dot(velocity, impulse_direction);

    // clamp normal impulse to the permissable range
    float dPn = effective_mass_normal * (-impulse_numerator + bias);
    float Pn0 = Pn;
    Pn = std::max(Pn0 + dPn, 0.0f);
    dPn = Pn - Pn0;

    vec3 impulse = impulse_direction *  dPn;
    // apply impulse to primary quantities
    body_a->cm_velocity -= impulse * body_a->one_over_mass;
    body_a->angular_momentum -= cross(ra, impulse);
    // compute affected auxillary quantities
    body_a->angular_velocity = body_a->inverse_world_inertia_tensor * body_a->angular_momentum;
    // apply impulse to primary quantities
    body_b->cm_velocity += impulse * body_b->one_over_mass;
    body_b->angular_momentum += cross(rb, impulse);
    // compute affected auxillary quantities
    body_b->angular_velocity = body_b->inverse_world_inertia_tensor * body_b->angular_momentum;

    ra = pa - body_a->cm_position;
    va = body_a->cm_velocity + cross(body_a->angular_velocity, ra);
    rb = pb - body_b->cm_position;
    vb = body_b->cm_velocity + cross(body_b->angular_velocity, rb);
    
    velocity = vb - va;
    impulse_direction = tangent;
    impulse_numerator = dot(velocity, impulse_direction);

    // clamp friction impulse to the permissable range
    float friction =  std::min(col_body_a->friction, col_body_b->friction);

    float dPt = - effective_mass_tangent * impulse_numerator;
    // Compute friction impulse
    float maxPt = friction * Pn;
    // Clamp friction
    float oldTangentImpulse = Pt;
    Pt = clamp(oldTangentImpulse + dPt, -maxPt, maxPt);
    dPt = Pt - oldTangentImpulse;

    // @todo fix friction curving
    // float friction = body_a->friction * body_b->friction;
    // float Pt0 = Pt;
    // float dPt = -effective_mass_tangent * impulse_numerator;
    // // Compute friction impulse
    // float maxPt = friction * Pn;
    // Pt += dPt;
    // Pt = clamp(Pt, -maxPt, maxPt);
    // dPt = Pt - Pt0;

    impulse = impulse_direction * dPt;
    // apply impulse to primary quantities
    body_a->cm_velocity -= impulse * body_a->one_over_mass;
    body_a->angular_momentum -= cross(ra, impulse);
    // compute affected auxillary quantities
    body_a->angular_velocity = body_a->inverse_world_inertia_tensor * body_a->angular_momentum;
    // apply impulse to primary quantities
    body_b->cm_velocity += impulse * body_b->one_over_mass;
    body_b->angular_momentum += cross(rb, impulse);
    // compute affected auxillary quantities
    body_b->angular_velocity = body_b->inverse_world_inertia_tensor * body_b->angular_momentum;

    ra = pa - body_a->cm_position;
    va = body_a->cm_velocity + cross(body_a->angular_velocity, ra);
    rb = pb - body_b->cm_position;
    vb = body_b->cm_velocity + cross(body_b->angular_velocity, rb);

    impulse_direction = bitangent;
    impulse_numerator = dot(velocity, impulse_direction);

    // clamp friction impulse to the permissable range
    friction =  std::min(col_body_a->friction, col_body_b->friction);
    float dPbt = -effective_mass_bitangent * impulse_numerator;
    // Compute friction impulse
    float maxPbt = friction * Pn;
    // Clamp friction
    float oldBitangentImpulse = Pbt;
    Pbt = clamp(oldBitangentImpulse + dPbt, -maxPbt, maxPbt);
    dPbt = Pbt - oldBitangentImpulse;

    // @todo fix friction curving
    // float Pbt0 = Pbt;
    // float dPbt = -effective_mass_bitangent * impulse_numerator;
    // // Compute friction impulse
    // float maxPbt = friction * Pn;
    // Pbt += dPbt;
    // Pbt = clamp(Pbt, -maxPbt, maxPbt);
    // dPbt = Pbt - Pbt0;

    impulse = impulse_direction * dPbt;
    // apply impulse to primary quantities
    body_a->cm_velocity -= impulse * body_a->one_over_mass;
    body_a->angular_momentum -= cross(ra, impulse);
    // compute affected auxillary quantities
    body_a->angular_velocity = body_a->inverse_world_inertia_tensor * body_a->angular_momentum;
    // apply impulse to primary quantities
    body_b->cm_velocity += impulse * body_b->one_over_mass;
    body_b->angular_momentum += cross(rb, impulse);
    // compute affected auxillary quantities
    body_b->angular_velocity = body_b->inverse_world_inertia_tensor * body_b->angular_momentum;

    return 0;
}


vector<Face> getMostParallelFace(const CollisionBody &hullA, vec3 n1) {
    float d = -FLT_MAX;
    vector<Face> parallel_faces = {};
    // return most anti-parallel face on hull to n
    for (unsigned i = 0; i < hullA.faces.size(); i++) {
/*        if (hullA.type == TRIANGLE && i == 1) { 
            continue;
        }*/
        Face f = hullA.faces.at(i);

        vec3 n2 = f.normal_ws;
        float alignment = dot(normalize(n1), normalize(n2));
        if (alignment >= d) {
            parallel_faces.push_back(f);
            d = alignment;
        }
    }
    if (parallel_faces.size()) {
        return {parallel_faces.at(parallel_faces.size()-1)};
    } else {
        return {};
    }
}

vector<Face> getMostAntiparallelFace(const CollisionBody &hullA, vec3 n1) {
    float d = FLT_MAX;
    vector<Face> antiparallel_faces = {};
    // return most anti-parallel face on hull to n
    for (unsigned i = 0; i < hullA.faces.size(); i++) {
/*        if (hullA.type == TRIANGLE && i == 1) { 
            continue;
        }*/
        Face f = hullA.faces.at(i);
        vec3 n2 = f.normal_ws;
        float alignment = dot(normalize(n1), normalize(n2));
        if (alignment <= d) {
            antiparallel_faces.push_back(f);
            d = alignment;
        }
    }
    if (antiparallel_faces.size()) {
        return {antiparallel_faces.at(antiparallel_faces.size()-1)};
    } else {
        return {};
    }
}

vector<Face> getOrthogonalFaces(const CollisionBody &hullA, vec3 n1) {

    vector<Face> orthogonal_faces = {};
    // return most anti-parallel face on hull to n
    for (unsigned i = 0; i < hullA.faces.size(); i++) {
        Face f = hullA.faces.at(i);
        vec3 n2 = f.normal_ws;
        float alignment = dot(normalize(n1), normalize(n2));
        if (fabs(alignment) < 0.98f){
            orthogonal_faces.push_back(f);
        }
    }
    return orthogonal_faces;
}

vector<Contact> sphereSphereContacts(CollisionBody &body_a,  CollisionBody &body_b) {
    vector<Contact> contacts = {};

    if (sphereSphereCollision(body_a, body_b)) {
        vec3 normal = normalize(body_b.cm_position - body_a.cm_position);
        vec3 p_a = body_a.cm_position + normal * body_a.dX2;
        vec3 p_b = body_b.cm_position - normal * body_b.dX2;
        vec3 collision_point = (p_a + p_b ) * 0.5f;
        float penetration = glm::distance(body_a.cm_position, body_b.cm_position) - (body_a.dX2 + body_b.dX2);

        if (penetration < depth_epsilon) {
            Contact contact(body_a, body_b);
            contact.penetration = penetration;
            contact.normal = normalize(normal);
            contact.collision_point = collision_point;
            contacts.push_back(contact);
        }
    }
    return contacts;
}

vector<Contact> sphereBoxContacts(CollisionBody &body_a,  CollisionBody &body_b) {
    vector<Contact> contacts = {};

    vec3 closest_pt = closestPointOnOBB(body_a.cm_position, body_b.dX2, body_b.dY2, body_b.dZ2, body_b.cm_position, body_b.orientation);
    if (sphereBoxCollision(body_a, closest_pt)) {
        vec3 normal = normalize(closest_pt - body_a.cm_position);
        vec3 p_a = body_a.cm_position + normal * body_a.dX2;
        vec3 p_b = closest_pt;
        float penetration = glm::distance(body_a.cm_position, closest_pt) - body_a.dX2;
        vec3 collision_point = (p_a + p_b) * 0.5f;
        if (penetration < depth_epsilon) {
            Contact contact(body_a, body_b);
            contact.penetration = penetration;
            contact.normal = normal;
            contact.collision_point = collision_point;
            contact.id = {ContactType::EDGE, body_a.id, body_b.id, 0,0,0,0,0,0,0};
            contacts.push_back(contact);
        }
    }
    return contacts;
}

vector<Contact> sphereCylinderContacts(CollisionBody &body_a,  CollisionBody &body_b) {

    vector<Contact> contacts = {};

    vec3 closest_pt = closestPointOnCylinder(body_a.cm_position, body_b);
    if (sphereCylinderCollision(body_a, closest_pt)) {
        vec3 normal = normalize(closest_pt - body_a.cm_position);
        vec3 p_a = body_a.cm_position + normal * body_a.dX2;
        vec3 p_b = closest_pt;
        float penetration = glm::distance(body_a.cm_position, closest_pt) - body_a.dX2;
        vec3 collision_point = (p_a + p_b) * 0.5f;
        if (penetration < depth_epsilon) {
            Contact contact(body_a, body_b);
            contact.penetration = penetration;
            contact.normal = normal;
            contact.collision_point = collision_point;
            contact.id = {ContactType::EDGE, body_a.id, body_b.id, 0,0,0,0,0,0,0};
            contacts.push_back(contact);
        }
    }

    return contacts;
}
void Swap(float &t1, float &t2) {
    float swap = t2;
    t2 = t1;
    t1 = swap;
}

struct AABB {
    vec3 min;
    vec3 max;
};
float EPSILON = 0.001f;
// Intersect ray R(t) = p + t*d against AABB a. When intersecting,
// return intersection distance tmin and point q of intersection
int IntersectRayAABB(vec3 p, vec3 d, AABB a, float &tmin, vec3 &q) {
    tmin = 0.0f; // // set to -FLT_MAX to get first hit on line
    float tmax = FLT_MAX; // set to max distance ray can travel (for segment)
    // For all three slabs
    for (int i = 0; i < 3; i++) {
        if (fabs(d[i]) < EPSILON) {
            // Ray is parallel to slab. No hit if origin not within slab
            if (p[i] < a.min[i] || p[i] > a.max[i]) return 0;
        } else {
            // Compute intersection t value of ray with near and far plane of slab
            float ood = 1.0f / d[i];
            float t1 = (a.min[i] - p[i]) * ood;
            float t2 = (a.max[i] - p[i]) * ood;
            // Make t1 be intersection with near plane, t2 with far plane
            if (t1 > t2) Swap(t1, t2);
            // Compute the intersection of slab intersection intervals
            if (t1 > tmin) tmin = t1;
            if (t2 > tmax) tmax = t2;
            // Exit with no collision as soon as slab intersection becomes empty
            if (tmin > tmax) return 0;
        }
    }
    // Ray intersects all 3 slabs. Return point (q) and intersection t value (tmin)
    q = p + d * tmin;
    return 1;
}

vector<Contact> hullHullContacts(CollisionBody &body_a,  CollisionBody &body_b) {


    vector<Contact> contacts = {};
    Query query;


    bool colliding = SAT(body_a, body_b, query);
    // don't bother creating contacts if the boxes aren't colliding
    if (!colliding) {
        body_a.colliding = body_b.colliding = false;
        return {};
    }
    body_a.colliding = body_b.colliding = true;

    CollisionBody *reference_hull;
    CollisionBody *incident_hull;

    if (query.id == 1) {
        // reference object A, incident object B
        reference_hull = &body_a;
        incident_hull = &body_b;
    }
    if (query.id == 2) {
        // reference object B, incident object A
        reference_hull = &body_b;
        incident_hull = &body_a;
    } else {
        // edge
    }

    if (query.id >= 1) {
        float dist = query.best_distance;
        ivec2 collision_pair_index =  query.collision_pair_index;
        vec3 normal = query.normal;
        if (incident_hull->type == TRIANGLE && dot(normal, incident_hull->faces.at(0).normal_ws) < 0.0f) {
            normal *= -1.0f;
        }
        vector<Face> reference_faces = getMostParallelFace(*reference_hull, normal);

        for (unsigned i = 0; i < reference_faces.size(); i++) {
            Face *reference_face = &reference_faces.at(i);

            vector<Face> incident_faces = getMostAntiparallelFace(*incident_hull, normal);

            vector<Face> side_faces = getOrthogonalFaces(*reference_hull, reference_face->normal_ws);

            for (unsigned j = 0; j < incident_faces.size(); j++) {

                Face *incident_face = &incident_faces.at(j);

                vector<vector<int>> contactIDs = {};
                // @todo fix for triangles and boxes ID prototype {ContactType, RHullID, IHullID, RFaceID, IFaceID, Edge0, Edge1, Plane0, Plane1, Plane2, Plane3}
                vector<int> id = {ContactType::FACE, reference_hull->id, incident_hull->id, reference_face->id, incident_face->id};
                for (unsigned int n = 0; n < incident_face->verts_indices.size(); n++) {

                    // start with a common id for each vertex on the plane about to be clipped
                    vector<int> currentID = id;    

                    // add the edge id that the vertex is at the start of
                    currentID.push_back(incident_face->verts_indices.at(n));

                    currentID.push_back(incident_face->verts_indices.at((n + 1) % incident_face->verts_indices.size()));

                    // add the side faces indexes
                    for (unsigned m = 0; m < side_faces.size(); m++) {
                        currentID.push_back(side_faces.at(m).id);
                    }
                    // add boolean value for whether point was clipped by that side face or not
                    for (unsigned m = 0; m < side_faces.size(); m++) {
                        currentID.push_back(0); 
                    }
                    contactIDs.push_back(currentID);

                }
                vector<vec3> clipped_polygon = incident_face->verts_ws;

                // clip incident face against side planes of reference face
                for (unsigned k = 0; k < side_faces.size(); k++) {
                    Face f = side_faces.at(k);
                    clipPolygonToPlane(clipped_polygon, f.verts_ws.at(0), f.normal_ws, contactIDs, k);  
                }

                assert(clipped_polygon.size() != 0);
                for (unsigned k = 0; k < clipped_polygon.size(); k++) {
                    vec3 possible_contact_point = clipped_polygon.at(k);
                    // keep all points below reference face plane
                    if (pointPlaneDistance(possible_contact_point, reference_face->position_ws, reference_face->normal_ws) < 0.0f) {
                        if (dot(normal, possible_contact_point - body_a.cm_position) < 0.0f) {
                            normal = normal * -1.0f;
                        }
                        Contact contact(body_a, body_b);
                        contact.collision_point = possible_contact_point;
                        contact.penetration = dist;
                        contact.normal = normalize(normal);
                        mat3 basis = formOrthogonalBasis(contact.normal);
                        contact.tangent = normalize(basis[1]);
                        contact.bitangent = normalize(basis[2]);
                        contact.id = contactIDs.at(k);
                        contacts.push_back(contact);
                    }
                }
            }
        }
    } else {

        float dist = query.best_distance;
        // edge collision
        int i = query.collision_pair_index.x;
        int j = query.collision_pair_index.y;
        if (i == -1 || j == -1) {
            return {};
        }
        Edge edge_a = body_a.edges.at(i);
        Edge edge_b = body_b.edges.at(j);

        vec2 mu = lineSegmentIntersection(edge_a.v1_ws, edge_a.v2_ws, edge_b.v1_ws, edge_b.v2_ws);
        float mu_a = mu.x;
        float mu_b = mu.y;
        bool valid_intersection = mu_a >= 0.0f && mu_a <= 1.0f && mu_b >= 0.0f && mu_b <= 1.0f;
        if (valid_intersection) {
            vec3 pt1 = edge_a.v1_ws + (edge_a.v2_ws - edge_a.v1_ws) * mu_a;
            vec3 pt2 = edge_b.v1_ws + (edge_b.v2_ws - edge_b.v1_ws) * mu_b;
            vec3 contact_point = (pt1 + pt2) * 0.5f;

            if (dot(query.normal, contact_point - body_a.cm_position) < 0.0f) {
                query.normal = query.normal * -1.0f;
            }
            Contact contact(body_a, body_b);
            contact.collision_point = contact_point;
            contact.penetration = dot(pt2 - pt1, query.normal);
            contact.normal = query.normal;
            mat3 basis = formOrthogonalBasis(contact.normal);
            contact.tangent = normalize(basis[1]);
            contact.bitangent = normalize(basis[2]);
            contact.id = {ContactType::EDGE, body_a.id, body_b.id, edge_a.id, edge_b.id};
            contacts.push_back(contact);

        }
        
    }

    return contacts;
}

vector<Contact> sphereTriContacts(CollisionBody &body_a, CollisionBody &body_b) {
    vector<Contact> contacts = {};    
    unsigned int contactType;
    vec3 p1 = pointTriangleIntersection(body_a.cm_position, body_b.a_bounding_vertices.at(0),  body_b.a_bounding_vertices.at(1),  body_b.a_bounding_vertices.at(2), contactType);
    vec3 normal = normalize(p1 - body_a.cm_position);
    vec3 p2 = body_a.cm_position + normal * body_a.dX2;
    float seperation = dot(p1 - p2, normal);
    if (seperation < 0.001f) {
        Contact contact(body_a, body_b);
        contact.collision_point = (p1 + p2) * 0.5f;
        contact.penetration = seperation;
        contact.normal = normal;
        mat3 basis = formOrthogonalBasis(contact.normal);
        contact.tangent = normalize(basis[1]);
        contact.bitangent = normalize(basis[2]);
        vector<int> id = {contactType, body_a.type, body_b.type, body_a.id, body_b.id};
        contact.id = id;
        contacts.push_back(contact);
    }
    return contacts;
}

vector<Contact> createContacts(CollisionBody &body_a,  CollisionBody &body_b) {
    CollisionBody *temp;
    CollisionBody *p_body_a = &body_a;
    CollisionBody *p_body_b = &body_b;

    // broad phase (bounding sphere collision) early out
    float r = (body_a.bounding_radius + body_b.bounding_radius);
    // if (glm::distance2(body_a.cm_position, body_b.cm_position) > r*r) {
    //     return {};
    // }

    if (p_body_a->one_over_mass == 0.0f && p_body_b->one_over_mass == 0.0f) {
        return {};
    }

    // ignore internal contacts in composite bodies
    if (p_body_a->compositeBodyIndex == p_body_b->compositeBodyIndex && p_body_a->compositeBodyIndex != -1) {
        return {};
    }

    if (body_a.type == BodyType::BOX && body_b.type == BodyType::BOX) {
        return hullHullContacts(*p_body_a, *p_body_b);
    }

    if (body_a.type == BodyType::BOX && body_b.type == BodyType::CYLINDER || 
        body_a.type == BodyType::CYLINDER && body_b.type == BodyType::BOX) {
        if (p_body_a->type == BodyType::CYLINDER && p_body_b->type == BodyType::BOX) {
            temp = p_body_a;
            p_body_a = p_body_b;
            p_body_b = temp;
        }
        return hullHullContacts(*p_body_a, *p_body_b);
    }

    if (body_a.type == BodyType::BOX && body_b.type == BodyType::TRIANGLE || 
        body_a.type == BodyType::TRIANGLE && body_b.type == BodyType::BOX) {
        if (p_body_a->type == BodyType::TRIANGLE && p_body_b->type == BodyType::BOX) {
            temp = p_body_a;
            p_body_a = p_body_b;
            p_body_b = temp;
        }

        return hullHullContacts(*p_body_a, *p_body_b);
    }

    if (body_a.type == BodyType::CYLINDER && body_b.type == BodyType::TRIANGLE || 
        body_a.type == BodyType::TRIANGLE && body_b.type == BodyType::CYLINDER) {
        if (p_body_a->type == BodyType::TRIANGLE && p_body_b->type == BodyType::CYLINDER) {
            temp = p_body_a;
            p_body_a = p_body_b;
            p_body_b = temp;
        }
        return hullHullContacts(*p_body_a, *p_body_b);
    }

    if (body_a.type == BodyType::TRIANGLE && body_b.type == BodyType::TRIANGLE) {
        return hullHullContacts(*p_body_a, *p_body_b);
    }

    if (body_a.type == BodyType::CYLINDER && body_b.type == BodyType::CYLINDER) {
        return hullHullContacts(*p_body_a, *p_body_b);
    }

    if (p_body_a->type == BodyType::SPHERE && p_body_b->type == BodyType::SPHERE) {
        return sphereSphereContacts(*p_body_a, *p_body_b);
    }

    if ((p_body_a->type == BodyType::SPHERE && p_body_b->type == BodyType::BOX) ||
        (p_body_a->type == BodyType::BOX && p_body_b->type == BodyType::SPHERE)) {

        if (p_body_a->type == BodyType::BOX && p_body_b->type == BodyType::SPHERE) {
            temp = p_body_a;
            p_body_a = p_body_b;
            p_body_b = temp;
        }
        return sphereBoxContacts(*p_body_a, *p_body_b);
    }

    if ((p_body_a->type == BodyType::SPHERE && p_body_b->type == BodyType::TRIANGLE) ||
        (p_body_a->type == BodyType::TRIANGLE && p_body_b->type == BodyType::SPHERE)) {

        if (p_body_a->type == BodyType::TRIANGLE && p_body_b->type == BodyType::SPHERE) {
            temp = p_body_a;
            p_body_a = p_body_b;
            p_body_b = temp;
        }
        return sphereTriContacts(*p_body_a, *p_body_b);
    }

    if ((p_body_a->type == BodyType::SPHERE && p_body_b->type == BodyType::CYLINDER) ||
        (p_body_a->type == BodyType::CYLINDER && p_body_b->type == BodyType::SPHERE)) {

        if (p_body_a->type == BodyType::CYLINDER && p_body_b->type == BodyType::SPHERE) {
            temp = p_body_a;
            p_body_a = p_body_b;
            p_body_b = temp;
        }
        return sphereCylinderContacts(*p_body_a, *p_body_b);
    }

    return {};
}

vector<Contact> createMinimalContacts(vector<Contact> &contacts) {

    vector<Contact> minimal_contacts = {};

    // first contact
    Contact *deepest_contact;
    float deepest_penetration = FLT_MAX;
    bool foundcontact1 = false;
    for (unsigned int i = 0; i < contacts.size(); i++)  {
        Contact *contact = &contacts.at(i);

        if (contact->penetration < deepest_penetration) {
            deepest_contact = contact;
            deepest_penetration = contact->penetration;
            foundcontact1 = true;
        }
    }   

    // second contact
    float max_dist = -FLT_MAX;
    Contact *furthest_contact1;
    bool foundcontact2 = false;
    for (unsigned int i = 0; i < contacts.size(); i++)  {
        Contact *contact = &contacts.at(i);
        float current_dist = distance(contact->collision_point, deepest_contact->collision_point);
        if (current_dist > max_dist && contact != deepest_contact) {
            furthest_contact1 = contact;
            max_dist = current_dist;
            foundcontact2 = true;
        }
    }

    // third contact
    max_dist = -FLT_MAX;
    Contact *furthest_contact2;
    bool foundcontact3 = false;
    for (unsigned int i = 0; i < contacts.size(); i++)  {
        Contact *contact = &contacts.at(i);
        float u = pointLineIntersection(contact->collision_point, furthest_contact1->collision_point, deepest_contact->collision_point);
        vec3 p = furthest_contact1->collision_point + (deepest_contact->collision_point-furthest_contact1->collision_point) * u;
        float current_dist = distance(p, contact->collision_point);
        if (current_dist > max_dist && contact != deepest_contact && contact != furthest_contact1) {
            furthest_contact2 = contact;
            max_dist = current_dist;
            foundcontact3 = true;
        }
    }        

    // fourth contact
    max_dist = -FLT_MAX;
    Contact *furthest_contact3;
    bool foundcontact4 = false;
    for (unsigned int i = 0; i < contacts.size(); i++)  {
        Contact *contact = &contacts.at(i);
        float d = distPointToTriangle(contact->collision_point,
                                      deepest_contact->collision_point,
                                      furthest_contact1->collision_point,
                                      furthest_contact2->collision_point);
        if (d > max_dist && contact != deepest_contact && contact != furthest_contact1 && contact != furthest_contact2) {
            furthest_contact3 = contact;
            max_dist = d;
            foundcontact4 = true;
        }
    }        

    if (foundcontact1) {
        minimal_contacts.push_back(*deepest_contact);
    }
    if (foundcontact2) {
        minimal_contacts.push_back(*furthest_contact1);
    }
    if (foundcontact3) {
        minimal_contacts.push_back(*furthest_contact2);
    }
    if (foundcontact4) {
        minimal_contacts.push_back(*furthest_contact3);
    }

    return minimal_contacts;    
}

int simulate(float deltaTime) {
    vector<Contact> contacts = Contact::allContacts;

    // integrate forces
    for (unsigned int i = 0; i < RigidBody::allBodies.size(); i++) {
        RigidBody *rigid_body = RigidBody::allBodies.at(i);
        rigid_body->integrateForces(rigid_body->cm_force, deltaTime);
    }
    // update collision body's
    for (unsigned int i = 0; i < CollisionBody::allBodies.size(); i++) {

        CollisionBody *collision_body = CollisionBody::allBodies.at(i);
        collision_body->update();
    }

    // create minimal contacts

    vector<Contact> current_contacts = {};

    for (unsigned int i = 0; i < CollisionBody::allBodies.size(); i++) {
        CollisionBody *body_a = CollisionBody::allBodies.at(i);

        for (unsigned int j = i + 1; j < CollisionBody::allBodies.size(); j++) {
            CollisionBody *body_b = CollisionBody::allBodies.at(j);

            vector<Contact> all_contacts = createContacts(*body_a, *body_b);
            vector<Contact> minimal_contacts = {};

            if (all_contacts.size() > 4) {
                vector<Contact> minimal_contacts = createMinimalContacts(all_contacts);
                current_contacts.insert( current_contacts.end(), minimal_contacts.begin(), minimal_contacts.end() );

            } else {
                current_contacts.insert( current_contacts.end(), all_contacts.begin(), all_contacts.end() );
            }               
        }
    }

    // contact caching
    for (unsigned int i = 0; i < current_contacts.size(); i++) {
        Contact *contact = &current_contacts.at(i);
        int n = 0;
        int _v = -1;
        for (unsigned int j = 0; j < contacts.size(); j++) {
            Contact *old_contact = &contacts.at(j);
            if ((contact->id == old_contact->id)) {
                n++;
                _v = i;

                contact->Pn = old_contact->Pn;
                vec3 friction = old_contact->tangent * old_contact->Pt + old_contact->bitangent * old_contact->Pbt;
                contact->Pt = dot(friction, contact->tangent);
                contact->Pbt = dot(friction, contact->bitangent);
                contact->warmStartNo = old_contact->warmStartNo;
                contact->warmStartNo++;
                if (contact->warmStartNo > warmStartRecord) {
                    warmStartRecord = contact->warmStartNo;
                    //std::cout << "Warm start record: " << warmStartRecord << std::endl;
                }
                break;
            }
        }
    }

    Contact::allContacts = current_contacts;

    // pre step
    for (unsigned int i = 0; i < Contact::allContacts.size(); i++) {
        Contact *contact = &Contact::allContacts.at(i);
        contact->preStep(deltaTime);
    }

    for (unsigned int i = 0; i < SphericalJoint::allJoints.size(); i++) {
        SphericalJoint *joint = SphericalJoint::allJoints.at(i);
        joint->preStep(deltaTime);
    }

    for (unsigned int i = 0; i < HingeJoint::allJoints.size(); i++) {
        HingeJoint *joint = HingeJoint::allJoints.at(i);
        joint->preStep(deltaTime);
    }

    for (unsigned int i = 0; i < MouseJoint::allJoints.size(); i++) {
        MouseJoint *joint = MouseJoint::allJoints.at(i);
        joint->preStep(deltaTime);
    }

    // apply impulses
    for (unsigned int i = 0; i < Contact::iterations; i++) {
        for (unsigned int j = 0; j < Contact::allContacts.size(); j++) {
            Contact *contact = &Contact::allContacts.at(j);
            contact->applyImpulse();
        }
        for (unsigned int j = 0; j < SphericalJoint::allJoints.size(); j++) {
            SphericalJoint *joint = SphericalJoint::allJoints.at(j);
            joint->applyImpulse();
        }
        
        for (unsigned int j = 0; j < HingeJoint::allJoints.size(); j++) {
            HingeJoint *joint = HingeJoint::allJoints.at(j);
            joint->applyImpulse();
        }
        for (unsigned int j = 0; j < MouseJoint::allJoints.size(); j++) {
            MouseJoint *joint = MouseJoint::allJoints.at(j);
            joint->applyImpulse();
        }
    }

    // integrate velocities
    for (unsigned int i = 0; i < RigidBody::allBodies.size(); i++) {
        RigidBody *rigid_body = RigidBody::allBodies.at(i);
        rigid_body->integrateVelocities(deltaTime);
        // update each of the components positions (no integration necessary)
        for (unsigned int j = 0; j < rigid_body->bodyIndexes.size(); j++) {
            unsigned int ind = rigid_body->bodyIndexes.at(j);
            CollisionBody *collision_body = CollisionBody::allBodies.at(ind);
            // with our updated composite body orientation
            // we update the local body transform (local to the composite body cm_position) with the new orientation
            mat4 model = translate(mat4(1.0f), collision_body->cm_position) * mat4(collision_body->orientation);
            mat4 composite = translate(mat4(1.0f), rigid_body->cm_position) * mat4(rigid_body->orientation);
            // convert back to world space
            mat4 m =  composite * collision_body->offset;
            collision_body->orientation = mat3(m);
            collision_body->cm_position = getPos(m);
        }
    }

    return 0;
}