#include "mouse_joint.hpp"

#define GLM_ENABLE_EXPERIMENTAL
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/string_cast.hpp>

vector<MouseJoint*> MouseJoint::allJoints = {};

MouseJoint::MouseJoint(unsigned int hullAIndex, vec3 anchorA, vec3 anchorB) {
    body_a_index = hullAIndex;
    anchor_a = anchorA;
    anchor_b = anchorB;
    anchor_a_ws = anchorA;
    anchor_b_ws =  anchorB;
    unset = true;
    MouseJoint::allJoints.push_back(this);
}

int MouseJoint::setAnchor(vec3 anchor) {
    unset = false;
    anchor_b = anchor;
    return 0;
}

int MouseJoint::setBodyAnchor(vec3 localPoint) {
    anchor_a = localPoint;
    return 0;
}

int MouseJoint::preStep(float dt) {
    if (unset) {
        return 0;
    }

    vec3 pa;
    vec3 ra;
    vec3 pb;
    vec3 rb;
    float one_over_mass;
    mat3 inverse_world_inertia_tensor;

    if (body_a->compositeBodyIndex == -1) {

        pa = body_a->cm_position + body_a->orientation * this->anchor_a;
        ra = pa - body_a->cm_position;
        pb = this->anchor_b;
        rb = pb;
        one_over_mass = body_a->one_over_mass;
        inverse_world_inertia_tensor = body_a->inverse_world_inertia_tensor;

    } else {
        RigidBody *compositeBody = RigidBody::allBodies.at(body_a->compositeBodyIndex);
        pa = compositeBody->cm_position + compositeBody->orientation * this->anchor_a;
        ra = pa - compositeBody->cm_position;
        pb = this->anchor_b;
        rb = pb;
        one_over_mass = compositeBody->one_over_mass;
        inverse_world_inertia_tensor = compositeBody->inverse_world_inertia_tensor;
    }

    float k_biasFactor = 0.2f;
    this->bias = -k_biasFactor * 1.0f / dt * (pb - pa);

    effective_mass = (one_over_mass) * mat3(1.0f)
                       - skewSymmetric(ra) * inverse_world_inertia_tensor * skewSymmetric(ra);


    effective_mass = glm::inverse(effective_mass);

    if (body_a->compositeBodyIndex == -1) {
        // apply impulse to primary quantities
        body_a->cm_velocity -= P * one_over_mass;
        body_a->angular_momentum -= cross(ra, P);
        // compute affected auxillary quantities
        body_a->angular_velocity = body_a->inverse_world_inertia_tensor * body_a->angular_momentum;
        // small damping amount
        body_a->angular_velocity *= 0.98f;
    } else {
        RigidBody *compositeBody = RigidBody::allBodies.at(body_a->compositeBodyIndex);
        // apply impulse to primary quantities
        compositeBody->cm_velocity -= P * one_over_mass;
        compositeBody->angular_momentum -= cross(ra, P);
        // compute affected auxillary quantities
        compositeBody->angular_velocity = compositeBody->inverse_world_inertia_tensor * compositeBody->angular_momentum;
        // small damping amount
        compositeBody->angular_velocity *= 0.98f;
    }
    return 0;
}

int MouseJoint::applyImpulse() {
    if (unset) {
        return 0;
    }
    vec3 pa;
    vec3 ra;
    vec3 va;
    vec3 pb;
    vec3 rb;
    vec3 vb;
    if (body_a->compositeBodyIndex == -1) {

        pa = body_a->cm_position + body_a->orientation * this->anchor_a;
        ra = pa - body_a->cm_position;
        pb = this->anchor_b;
        rb = pb;
        va = body_a->cm_velocity + cross(body_a->angular_velocity, ra);
        vb = anchor_b - prev_anchor_b;
    } else {
        RigidBody *compositeBody = RigidBody::allBodies.at(body_a->compositeBodyIndex);
        pa = compositeBody->cm_position + compositeBody->orientation * this->anchor_a;
        ra = pa - compositeBody->cm_position;
        pb = this->anchor_b;
        rb = pb;
        va = compositeBody->cm_velocity + cross(compositeBody->angular_velocity, ra);
        vb = anchor_b - prev_anchor_b;
    }

    prev_anchor_b = anchor_b;

    anchor_a_ws = pa;
    anchor_b_ws = pb;

    vec3 rel_vel = vb - va;
    vec3 impulse = effective_mass * (-rel_vel + this->bias);
    if (body_a->compositeBodyIndex == -1) {
        // apply impulse to primary quantities
        body_a->cm_velocity -= impulse * body_a->one_over_mass;
        body_a->angular_momentum -= cross(ra, impulse);
        // compute affected auxillary quantities
        body_a->angular_velocity = body_a->inverse_world_inertia_tensor * body_a->angular_momentum;
        // small damping amount
        body_a->angular_velocity *= 0.98f;
    }
    else {
        RigidBody *compositeBody = RigidBody::allBodies.at(body_a->compositeBodyIndex);
        // apply impulse to primary quantities
        compositeBody->cm_velocity -= impulse * compositeBody->one_over_mass;
        compositeBody->angular_momentum -= cross(ra, impulse);
        // compute affected auxillary quantities
        compositeBody->angular_velocity = compositeBody->inverse_world_inertia_tensor * compositeBody->angular_momentum;
        // small damping amount
        compositeBody->angular_velocity *= 0.98f;
    }

    P += impulse;

    return 0;
}