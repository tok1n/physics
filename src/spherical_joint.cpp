#include "spherical_joint.hpp"

#define GLM_ENABLE_EXPERIMENTAL
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/string_cast.hpp>

vector<SphericalJoint*> SphericalJoint::allJoints = {};

SphericalJoint::SphericalJoint(unsigned int hullAIndex, vec3 anchorA,  unsigned int hullBIndex,  vec3 anchorB) {
    body_a_index = hullAIndex;
    body_b_index = hullBIndex;
    anchor_a = anchorA;
    anchor_b = anchorB;
    anchor_a_ws = anchorA;
    anchor_b_ws = anchorB;
    unset = true;
    SphericalJoint::allJoints.push_back(this);
}


int SphericalJoint::preStep(float dt) {

    body_a = RigidBody::allBodies.at(body_a_index);
    body_b = RigidBody::allBodies.at(body_b_index);

    vec3 pa = body_a->cm_position + body_a->orientation * this->anchor_a;
    vec3 ra = pa - body_a->cm_position;
    vec3 pb = body_b->cm_position + body_b->orientation * this->anchor_b;
    vec3 rb = pb - body_b->cm_position;

    float k_biasFactor = 0.1f;
    this->bias = -k_biasFactor * 1.0f / dt * (pb - pa);

    effective_mass = (body_a->one_over_mass + body_b->one_over_mass) * mat3(1.0f)
                       - skewSymmetric(ra) * body_a->inverse_world_inertia_tensor * skewSymmetric(ra)
                       - skewSymmetric(rb) * body_b->inverse_world_inertia_tensor * skewSymmetric(rb);

    effective_mass = glm::inverse(effective_mass);

    // apply impulse to primary quantities
    body_a->cm_velocity -= P * body_a->one_over_mass;
    body_a->angular_momentum -= cross(ra, P);
    // compute affected auxillary quantities
    body_a->angular_velocity = body_a->inverse_world_inertia_tensor * body_a->angular_momentum;

    // apply impulse to primary quantities
    body_b->cm_velocity += P * body_b->one_over_mass;
    body_b->angular_momentum += cross(rb, P);
    // compute affected auxillary quantities
    body_b->angular_velocity = body_b->inverse_world_inertia_tensor * body_b->angular_momentum;

    return 0;
}

int SphericalJoint::applyImpulse() {

    vec3 pa = body_a->cm_position + body_a->orientation * this->anchor_a;
    vec3 ra = pa - body_a->cm_position;
    vec3 va = body_a->cm_velocity + cross(body_a->angular_velocity, ra);
    vec3 pb = body_b->cm_position + body_b->orientation * this->anchor_b;
    vec3 rb = pb - body_b->cm_position;
    vec3 vb = body_b->cm_velocity + cross(body_b->angular_velocity, rb);

    anchor_a_ws = pa;
    anchor_b_ws = pb;

    vec3 rel_vel = vb - va;
    vec3 impulse = effective_mass * (-rel_vel + this->bias);

    // apply impulse to primary quantities
    body_a->cm_velocity -= impulse * body_a->one_over_mass;
    body_a->angular_momentum -= cross(ra, impulse);
    // compute affected auxillary quantities
    body_a->angular_velocity = body_a->inverse_world_inertia_tensor * body_a->angular_momentum;

    // apply impulse to primary quantities
    body_b->cm_velocity += impulse * body_b->one_over_mass;
    body_b->angular_momentum += cross(rb, impulse);
    // compute affected auxillary quantities
    body_b->angular_velocity = body_b->inverse_world_inertia_tensor * body_b->angular_momentum;

    P += impulse;

    return 0;
}