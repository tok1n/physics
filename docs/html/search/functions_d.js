var searchData=
[
  ['query',['Query',['../struct_query.html#a4c1633236bdb9fa8d3fd3572a469889d',1,'Query::Query()'],['../struct_query.html#aa8f8d5daaf7ee07a7429a832e8bf7351',1,'Query::Query(float _best_distance, glm::ivec2 _collision_pair_index, glm::vec3 _normal)'],['../struct_query.html#af7d0f85bcec07233387a3377dfcec3a4',1,'Query::Query(const Query &amp;q)']]],
  ['queryedgedirections',['queryEdgeDirections',['../collision_8hpp.html#a5924e1ded04f6d8109d72c71ab00b35d',1,'collision.hpp']]],
  ['queryfacedirections',['queryFaceDirections',['../collision_8hpp.html#a0e53a509263a61b68661f9ab810c127b',1,'collision.hpp']]]
];
