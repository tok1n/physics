var indexSectionsWithContent =
{
  0: "abcdefghilmnopqrstuvwxyz~",
  1: "cefhmqrst",
  2: "bchmps",
  3: "abcdefghilmopqrstu~",
  4: "abcdefghimnopstuvwxyz",
  5: "bcq",
  6: "bcefnst",
  7: "g",
  8: "t"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "enums",
  6: "enumvalues",
  7: "defines",
  8: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Files",
  3: "Functions",
  4: "Variables",
  5: "Enumerations",
  6: "Enumerator",
  7: "Macros",
  8: "Pages"
};

